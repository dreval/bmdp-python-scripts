#!/usr/bin/env python3
# -*- coding: utf-8 -*-

__author__ = 'Dimitri Scheftelowitsch'

import numpy as np


class MultiObjectiveAction:
    """
    A class for actions with multi-objective rewards
    """
    reward = np.array([])
    base_vector = np.array([])

    def __init__(self, reward, base):
        self.reward = reward
        self.base_vector = base

    def evaluate(self):
        return self.reward, self.base_vector


class MarkovDecisionProcess:
    """
    An abstract class for Markov decision processes
    """
    decision_table = []
    actions = []

    def __init__(self, actions):
        self.actions = actions
        self.decision_table = [0 for _ in range(len(actions))]

    def decide(self, state, action_index):
        """
        Set an individual decision in a given state

        @param state the state we are operating on
        @param action_index the index of the action we would like to perform
        """
        self.decision_table[state] = action_index

    def decision(self, state):
        """
        Return a decision in a given state
        @param state the state for a decision
        @return the decision in the state
        """
        return self.decision_table[state]

    def build_matrix(self):
        """
        Compute the transition matrix
        @return the matrix of the resulting Markov chain
        """
        return np.array([self.actions[i][self.decision_table[i]] for i in range(len(self.actions))])


class SingleObjectiveMarkovDecisionProcess(MarkovDecisionProcess):
    def reachable(self, state):
        """
        Computes reachable states.
        @param state The starting state
        @return A set of all reachable states
        """
        rstates = set()
        rest = [state]

        while len(rest) > 0:
            s = rest.pop()
            rstates.add(s)
            # print("work: {1} reachable: {2} next: {0}".format(s, rest, rstates))
            for i, (_, p) in enumerate(self.actions[s]):
                for j, _ in enumerate(self.actions):
                    if p[0, j] > 0 and not(j in rstates or j in rest or j == s):
                        rest.append(j)
        return rstates


    # assume that all actions are just pairs of (reward, p_vector)
    def value_iteration_step(self, gamma, values, policy, maximize=True):
        """
        Computes an intermediate step of value iteration
        @param gamma The discounting factor
        @param values Intermediary value vector
        @param policy Intermidate policy array, can be changed
        @param maximize The optimization direction, true if maximizing, minimizing otherwise
        @return the value vector, the next policy, the q-values, the maximal difference
        """
        delta = 0
        q_values = np.array([None for _ in self.actions])
        for (s, acts) in enumerate(self.actions):
            max_value = values[s]
            action = 0
            q_values[s] = [r + gamma * (p * np.matrix(values).T)[0, 0] for (r, p) in acts]
            for (i, _) in enumerate(acts):
                value = q_values[s][i]
                if maximize == (value >= max_value):
                    max_value = value
                    action = i
            delta = max(delta, abs(values[s] - max_value))
            values[s] = max_value
            policy[s] = action
        return values, policy, q_values, delta

    def value_iteration(self, gamma, epsilon, maximize=True):
        """
        Computes the optimal policy wrt expected discounted total reward
        @param gamma The discounting factor
        @param epsilon Precision limit
        @param maximize The optimization direction, true if maximizing, minimizing otherwise
        @return the value vector, an optimal policy and q-values
        """
        values = np.array([0.0 for _ in self.actions])
        policy = np.array([0 for _ in self.actions])
        q_values = None
        delta = epsilon + 1

        while delta > epsilon:
            values, policy, q_values, delta = self.value_iteration_step(gamma, values, policy, maximize)

        return values, policy, q_values

    def policy_evaluation(self, gamma, epsilon):
        """
        Computes the value of a given policy. The policy is given implicitly in decision_table
        @param gamma The discounting factor
        @param epsilon The minimal change to be considered
        @return the value vector
        """
        def policy_evaluation_step(gamma, values):
            """
            Local function to compute one step of iterative policy evaluation
            @param gamma Discounting factor
            @param values Intermediary value vector
            @return the new value vector and a maximal difference
            """
            delta = 0
            for s, acts in enumerate(self.actions):
                r, p = acts[self.decision_table[s]]
                v = r + gamma * (p * np.matrix(values).T)[0, 0]
                # print("{0} {5} = {1} + {2} * {3} * {4}".format(s, r, gamma, p, values, v))
                delta = max(delta, abs(v - values[s]))
                values[s] = v
            return values, delta

        local_delta = 1 + epsilon
        values = np.array([0.0 for _ in self.actions])
        while local_delta > epsilon:
            values, local_delta = policy_evaluation_step(gamma, values)

        return values

class MultiObjectiveMarkovDecisionProcess(MarkovDecisionProcess):
    """
    A class for multi-objective Markov decision processes
    """
    epsilon = 0.00001

    def value_iteration(self, gamma):
        sample_reward_vector, _ = self.actions[0][0].evaluate()
        for i in range(len(sample_reward_vector)):
            value = np.transpose(np.array([0 for _ in self.actions]))
            step = np.infty
            while step >= self.epsilon:
                step = 0
                for s in range(len(self.actions)):
                    q_max = 0
                    a_max = 0
                    for a in range(len(s)):
                        reward, transition = self.actions[0][0].evaluate()
                        q = reward[i] + gamma * transition * value
                        if q > q_max:
                            a_max = a
                            q_max = q

                    if q_max > value[s]:
                        self.decide(s, a_max)
                        step += abs(q_max - s)

            yield value

    def multi_objective_value_iteration(self, gamma):
        step = np.infty
        max_actions = max([len(a) for a in self.actions])
        r, _ = self.actions[0][0].evaluate()
        value = np.transpose(np.array([r for _ in self.actions]))
        idealized_values = self.value_iteration(gamma)
        e = 0.1

        def psi(x, r, l):
            arr = [l[i] * abs(r[i] - x[i]) for i in range(len(self.actions))]
            return max(arr) + e * sum(arr)

        while step >= self.epsilon:
            step = 0
            q = np.array([[r for _ in range(max_actions)] for _ in self.actions])
            for s in range(len(self.actions)):
                min_dist = np.infty
                for a in range(len(s)):
                    reward, transition = self.actions[0][0].evaluate()
                    q[s, a] = reward + gamma * transition * value
                    distance = psi(q[s, a], idealized_values[s], [1.0 for _ in r])
                    if min_dist > distance:
                        min_dist = distance
                        self.decide(s, a)

                d = abs(value[s] - min_dist)
                step += d
                value[s] = min_dist


class PolicyRewardTuple:
    """
    A class that stores a stationary policy and its (not necessarily completely computed) value vector
    """
    decisions = []
    rewards = []
    size = 0
    dimensions = 0

    def __init__(self, size, dimensions):
        self.size = size
        self.dimensions = dimensions
        self.decisions = [0 for _ in range(size)]
        self.rewards = np.array([[0.0 for _ in range(dimensions)] for _ in range(size)])

    def dominates(self, other):
        assert isinstance(other, PolicyRewardTuple)
        equal = self.size == other.size and self.dimensions == other.dimensions
        dominates = True
        if equal:
            dominates = (self.rewards >= other.rewards).all() and (self.rewards > other.rewards).any()
            for i in range(self.size):
                for j in range(self.dimensions):
                    if not(self.rewards[i, j] >= other.rewards[i, j]):
                        dominates = False
        return equal and dominates
