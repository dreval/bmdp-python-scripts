#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import numpy as np

import mdp

__author__ = 'Dimitri Scheftelowitsch'


class StochasticGame(mdp.MarkovDecisionProcess):
    """
    A class for (two-player zero-sum) stochastic games. Generally built upon Markov decision processes
    """

    def value_iteration(self, gamma, precision, maximize = True):
        policy = np.array([(0, 0) for _ in self.actions])
        values = np.array([0.0 for _ in self.actions])

        def value_iteration_step():
            delta = 0
            for s, acts in enumerate(self.actions):
                opt_value = values[s]
                p1_action = 0
                p2_action = 0
                for i, a in enumerate(acts): # actions = [ s_1 = [ a_1 = (4, [ [0.5, 0.5], [1, 0] ]), a_2 = [...] ], s_2 = ... ]
                    pes_value = values[s]
                    for j, (r, p) in enumerate(a):
                        value = r + gamma * np.sum(p * np.matrix(values).T)
                        if maximize == (value <= pes_value) or j == 0:
                            pes_value = value
                            p2_action = j
                    if maximize == (pes_value >= opt_value) or i == 0:
                        opt_value = pes_value
                        p1_action = i
                policy[s] = (p1_action, p2_action)
                dv = abs(values[s] - opt_value)
                delta = max(delta, dv)
                values[s] = opt_value
            return delta

        delta = precision + 1

        while delta > precision:
            delta = value_iteration_step()

        return values, policy