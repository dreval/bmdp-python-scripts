#!/usr/bin/env python3
# -*-coding: utf-8-*-

import numpy as np
import numpy.random as nr
from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from optparse import OptionParser
import polytope as pc
from numpy.core.umath import maximum


def covered_volume(points, minimal_coords, maximal_coords):
    # so, computing a volume of a 20+-dimensional union of cubes is not very efficient
    # (shame on me for thinking otherwise)
    # hence, let us think about Monte-Carlo volume computation methods.
    l = len(points)
    N = 1000000
    hits = 0
    dimensions = len(points[0])

    box_dims = [(maximal_coords[d] - minimal_coords[d]) for d in range(len(minimal_coords))]
    total_volume = np.prod(box_dims)
    # print("box: {0}".format(total_volume))

    for i in range(N):
        point_hit = False
        random_point = nr.random_sample(dimensions)
        random_point *= (maximal_coords - minimal_coords)
        random_point += minimal_coords

        for j, point in enumerate(points[:]):
            #print(minimal_coords)
            #print(random_point)
            #print(maximal_coords)
            #print((minimal_coords <= random_point).all())
            #print((maximal_coords >= random_point).all())
            #print(point)
            #print(random_point <= point)
            hit = (random_point <= point).all()
            if hit:
                point_hit = True
        if point_hit:
            hits += 1
    probability = float(hits) / N
    # print("prob: {0}, hits: {1}".format(probability, hits))

    return probability * total_volume


def c_measure(first_front, second_front):
    dominated_points = 0
    for point2 in second_front:
        for point1 in first_front:
            is_dominated = (point2 <= point1).all() # and (point2 < point1).any()
            if is_dominated:
                dominated_points += 1
                break
    return dominated_points, (float(dominated_points) / len(second_front))

if __name__ == "__main__":
    parser = OptionParser(usage="usage: %prog [-v, --volume-measure] [-c, --c-measure] [-V, --verbose] file1 file2")
    parser.add_option("-v", "--volume-measure", action="store_true", dest="volume_measure")
    parser.add_option("-c", "--c-measure", action="store_true", dest="c_measure")
    parser.add_option("-V", "--verbose", action="store_true", dest="verbose")
    (options, args) = parser.parse_args()
    np.random.random()

    first_file_name = args[0]
    second_file_name = args[1]
    first_data = np.loadtxt(first_file_name)
    second_data = np.loadtxt(second_file_name)

    first_points = first_data[:, 1:]
    second_points = second_data[:, 1:]

    output_string = ""

    if options.c_measure:
        points_1, c_first_second = c_measure(first_points, second_points)
        points_2, c_second_first = c_measure(second_points, first_points)

        if options.verbose:
            print("C({1}, {2}) = {0}".format(c_first_second, first_file_name, second_file_name))
            print("C({2}, {1}) = {0}".format(c_second_first, first_file_name, second_file_name))

        output_string += "{0}; {1}; {2}; {3}; {4}; {5}".format(c_first_second, c_second_first, points_1, len(second_points), points_2, len(first_points))
        if options.volume_measure:
            output_string += "; "

    if options.volume_measure:
        minimal_first_coords = [np.amin(first_points[:, i]) for i in range(first_points.shape[1])]
        minimal_second_coords = [np.amin(second_points[:, i]) for i in range(second_points.shape[1])]
        maximal_first_coords = [np.amax(first_points[:, i]) for i in range(first_points.shape[1])]
        maximal_second_coords = [np.amax(second_points[:, i]) for i in range(second_points.shape[1])]
        minimal_coords = np.minimum(minimal_first_coords, minimal_second_coords) - 1
        maximal_coords = np.maximum(maximal_first_coords, maximal_second_coords) + 0.1

        if options.verbose:
            print("Computing first volume…")

        vol_1 = covered_volume(first_points, minimal_coords, maximal_coords)

        if options.verbose:
            print(vol_1)
            print("Computing second volume…")

        vol_2 = covered_volume(second_points, minimal_coords, maximal_coords)

        if options.verbose:
            print(vol_2)

        output_string += "{0}; {1}".format(vol_1, vol_2)

    print(output_string)