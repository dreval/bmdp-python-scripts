from errno import EINVAL
from optparse import OptionParser

import cmdp

if __name__ == "__main__":
    parser = OptionParser(
        usage="usage: %prog -m,--mode=MODE --scenarios=num_scenarios --states=num_states --actions=num_actions")
    parser.add_option("-m", "--mode", action="store", type="str", dest="mode", default="random")
    parser.add_option("-s", "--scenarios", action="store", type="int", dest="scenarios")
    parser.add_option("-n", "--states", action="store", type="int", dest="states")
    parser.add_option("-a", "--actions", action="store", type="int", dest="actions")

    (options, args) = parser.parse_args()

    if not(options.mode in ["deterministic", "random"]):
        print("Error: Mode {} is not defined! Mode can be either `deterministic' or `random'".format(options.mode))
        exit(-EINVAL)

    if options.mode == "deterministic":
        deterministic = True
    elif options.mode == "random":
        deterministic = False

    model = cmdp.generate_random_cmdp(options.scenarios, options.states, options.actions, deterministic)
    print(model.dump_as_string())