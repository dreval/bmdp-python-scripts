#!/usr/bin/env python3
# -*- coding: utf-8 -*-

__author__ = 'Dimitri Scheftelowitsch'

from imdp import *
from datetime import datetime
import matplotlib.pyplot as plt
import numpy as np
import scipy.sparse as sp

# use the example from the paper

# basic rewards
new_basic_reward = 100.0
good_basic_reward = 90.0
adequate_basic_reward = 75.0
obsolete_basic_reward = 50.0
unusable_basic_reward = 10.0

# action: ignore
ignore_rewards = [0.9, 1.1, 1.0]  # lower, upper, expected

new_ignore_rewards = [new_basic_reward * x for x in ignore_rewards]
new_ignore = IntervalAction((new_ignore_rewards[0], new_ignore_rewards[1]),
                            (np.array([0.45, 0.1, 0.0, 0.0, 0.0]), np.array([0.55, 0.5, 0.0, 0.0, 0.1])),
                            np.matrix([[0.5, 0.45, 0.0, 0.0, 0.05]]), new_ignore_rewards[2])

good_ignore_rewards = [good_basic_reward * x for x in ignore_rewards]
good_ignore = IntervalAction((good_ignore_rewards[0], good_ignore_rewards[1]),
                             (np.array([0.0, 0.4, 0.0, 0.0, 0.0]), np.array([0.0, 0.6, 0.5, 0.0, 0.1])),
                             np.matrix([[0.0, 0.5, 0.45, 0.0, 0.05]]), good_ignore_rewards[2])

adequate_ignore_rewards = [adequate_basic_reward * x for x in ignore_rewards]
adequate_ignore = IntervalAction((adequate_ignore_rewards[0], adequate_ignore_rewards[1]),
                                 (np.array([0.0, 0.0, 0.3, 0.05, 0.0]), np.array([0.0, 0.0, 0.9, 0.7, 0.2])),
                                 np.matrix([[0.0, 0.0, 0.5, 0.4, 0.1]]), adequate_ignore_rewards[2])

obsolete_ignore_rewards = [obsolete_basic_reward * x for x in ignore_rewards]
obsolete_ignore = IntervalAction((obsolete_ignore_rewards[0], obsolete_ignore_rewards[1]),
                                 (np.array([0.0, 0.0, 0.0, 0.1, 0.4]), np.array([0.0, 0.0, 0.0, 0.6, 0.9])),
                                 np.matrix([[0.0, 0.0, 0.0, 0.3, 0.7]]), obsolete_ignore_rewards[2])

unusable_ignore_rewards = [unusable_basic_reward * x for x in ignore_rewards]
unusable_ignore = IntervalAction((unusable_ignore_rewards[0], unusable_ignore_rewards[1]),
                                 (np.array([0.0, 0.0, 0.0, 0.0, 1.0]), np.array([0.0, 0.0, 0.0, 0.0, 1.0])),
                                 np.matrix([[0.0, 0.0, 0.0, 0.0, 1.0]]), unusable_ignore_rewards[2])

# action: repair
repair_rewards = [0.75, 1.05, 0.9]

adequate_repair_rewards = [adequate_basic_reward * x for x in repair_rewards]
adequate_repair = IntervalAction((adequate_repair_rewards[0], adequate_repair_rewards[1]),
                                 (np.array([0.0, 0.6, 0.1, 0.0, 0.0]), np.array([0.0, 0.8, 0.3, 0.0, 0.1])),
                                 np.matrix([[0.0, 0.7, 0.25, 0.0, 0.05]]), adequate_repair_rewards[2])

obsolete_repair_rewards = [obsolete_basic_reward * x for x in repair_rewards]
obsolete_repair = IntervalAction((obsolete_repair_rewards[0], obsolete_repair_rewards[1]),
                                 (np.array([0.0, 0.0, 0.2, 0.5, 0.0]), np.array([0.0, 0.0, 0.3, 0.7, 0.2])),
                                 np.matrix([[0.0, 0.0, 0.25, 0.6, 0.15]]), obsolete_repair_rewards[2])

unusable_repair_rewards = [unusable_basic_reward * x for x in repair_rewards]
unusable_repair = IntervalAction((unusable_repair_rewards[0], unusable_repair_rewards[1]),
                                 (np.array([0.0, 0.0, 0.0, 0.7, 0.2]), np.array([0.0, 0.0, 0.0, 0.8, 0.3])),
                                 np.matrix([[0.0, 0.0, 0.0, 0.75, 0.25]]), unusable_repair_rewards[2])

# action: light overhaul
light_rewards = [0.6, 0.95, 0.85]

adequate_light_rewards = [adequate_basic_reward * x for x in light_rewards]
adequate_light = IntervalAction((adequate_light_rewards[0], adequate_light_rewards[1]),
                                (np.array([0.0, 0.85, 0.02, 0.0, 0.03]), np.array([0.0, 0.95, 0.08, 0.0, 0.07])),
                                np.matrix([[0.0, 0.93, 0.02, 0.0, 0.05]]), adequate_light_rewards[2])

obsolete_light_rewards = [obsolete_basic_reward * x for x in light_rewards]
obsolete_light = IntervalAction((obsolete_light_rewards[0], obsolete_light_rewards[1]),
                                (np.array([0.0, 0.2, 0.4, 0.1, 0.03]), np.array([0.0, 0.5, 0.6, 0.3, 0.08])),
                                np.matrix([[0.0, 0.35, 0.45, 0.15, 0.05]]), obsolete_light_rewards[2])

unusable_light_rewards = [unusable_basic_reward * x for x in light_rewards]
unusable_light = IntervalAction((unusable_light_rewards[0], unusable_light_rewards[1]),
                                (np.array([0.0, 0.0, 0.3, 0.4, 0.05]), np.array([0.0, 0.0, 0.5, 0.6, 0.15])),
                                np.matrix([[0.0, 0.0, 0.4, 0.5, 0.1]]), unusable_light_rewards[2])

# action: overhaul
overhaul_rewards = [0.5, 0.85, 0.8]

adequate_overhaul_rewards = [adequate_basic_reward * x for x in overhaul_rewards]
adequate_overhaul = IntervalAction((adequate_overhaul_rewards[0], adequate_overhaul_rewards[1]),
                                   (np.array([0.0, 0.9, 0.0, 0.0, 0.02]), np.array([0.0, 0.98, 0.0, 0.0, 0.1])),
                                   np.matrix([[0.0, 0.95, 0.0, 0.0, 0.05]]), adequate_overhaul_rewards[2])

obsolete_overhaul_rewards = [obsolete_basic_reward * x for x in overhaul_rewards]
obsolete_overhaul = IntervalAction((obsolete_overhaul_rewards[0], obsolete_overhaul_rewards[1]),
                                   (np.array([0.0, 0.75, 0.0, 0.0, 0.01]), np.array([0.0, 0.95, 0.2, 0.0, 0.09])),
                                   np.matrix([[0.0, 0.85, 0.1, 0.0, 0.05]]), obsolete_overhaul_rewards[2])

unusable_overhaul_rewards = [unusable_basic_reward * x for x in overhaul_rewards]
unusable_overhaul = IntervalAction((unusable_overhaul_rewards[0], unusable_overhaul_rewards[1]),
                                   (np.array([0.0, 0.0, 0.4, 0.2, 0.04]), np.array([0.0, 0.0, 0.8, 0.5, 0.06])),
                                   np.matrix([[0.0, 0.0, 0.6, 0.35, 0.05]]), unusable_overhaul_rewards[2])

# action: buy a new one
new_buy_new = IntervalAction((0.0, 0.0), (np.array([1.0, 0.0, 0.0, 0.0, 0.0]), np.array([1.0, 0.0, 0.0, 0.0, 0.0])),
                             np.matrix([[1.0, 0.0, 0.0, 0.0, 0.0]]), 0.0)
good_buy_new = IntervalAction((0.0, 0.0), (np.array([1.0, 0.0, 0.0, 0.0, 0.0]), np.array([1.0, 0.0, 0.0, 0.0, 0.0])),
                              np.matrix([[1.0, 0.0, 0.0, 0.0, 0.0]]), 0.0)
adequate_buy_new = IntervalAction((0.0, 0.0),
                                  (np.array([1.0, 0.0, 0.0, 0.0, 0.0]), np.array([1.0, 0.0, 0.0, 0.0, 0.0])),
                                  np.matrix([[1.0, 0.0, 0.0, 0.0, 0.0]]), 0.0)
obsolete_buy_new = IntervalAction((0.0, 0.0),
                                  (np.array([1.0, 0.0, 0.0, 0.0, 0.0]), np.array([1.0, 0.0, 0.0, 0.0, 0.0])),
                                  np.matrix([[1.0, 0.0, 0.0, 0.0, 0.0]]), 0.0)
unusable_buy_new = IntervalAction((0.0, 0.0),
                                  (np.array([1.0, 0.0, 0.0, 0.0, 0.0]), np.array([1.0, 0.0, 0.0, 0.0, 0.0])),
                                  np.matrix([[1.0, 0.0, 0.0, 0.0, 0.0]]), 0.0)

process = IntervalMarkovDecisionProcess(
    [[new_ignore, new_buy_new],
     [good_ignore, good_buy_new],
     [adequate_ignore, adequate_repair, adequate_light, adequate_overhaul, adequate_buy_new],
     [obsolete_ignore, obsolete_repair, obsolete_light, obsolete_overhaul, obsolete_buy_new],
     [unusable_ignore, unusable_repair, unusable_light, unusable_overhaul, unusable_buy_new]])


gamma = 0.98  # assume 2% per-step inflation

ivi_test = False
strange_test = False
compromise = False
pure_pareto_optimal = False
pareto_optimal = True
weight_optimal = False
benchmark = False
real_world = False
dump = False

expected_solution, lower_solution, upper_solution = None, None, None
pure_solutions = None
pareto_solutions = None
weight_solutions = None
real_results = None


def print_pure_solution_table(pi_set):
    assert isinstance(pi_set, list)
    format_block = "|".join(["l" for _ in pi_set])
    table_header = "\\begin{tabular}{" + format_block + "}"
    table_footer = "\\end{tabular}"
    table_contents = "\\ \n".join(["&".join(["{0}".format(pi.rewards[s]) for pi in pi_set]) for s in range(5)])
    table_head = "&".join(["{0}".format(pi.decisions) for pi in pi_set])
    return "\n".join([table_header, table_footer, table_head, table_contents])

if __name__ == "__main__":
    if ivi_test:
        print("Testing numerical stability...")
        upper_vi, lower_vi, expected_vi, policy = process.tri_objective_value_iteration(gamma, 0.0, 1.0)
        process_prime = IntervalMarkovDecisionProcess([[process.actions[i][policy[i]]] for i in range(len(process.actions))])
        pv = PolicyRewardTuple(len(process.actions), 2)
        pv.decisions = policy
        process.evaluate_policy(pv, gamma)
        # half-baked ivi
        upper, lower, expected = upper_vi, lower_vi, expected_vi
        for k in range(980):
            upper, lower, expected, _ = \
                process_prime.tri_objective_interval_value_iteration_step(gamma, 0.0, 1.0, upper, lower, expected)
        # compute errors and other fine stuff
        expected_pe = np.matrix(pv.rewards[:, 0]).T
        lower_pe = np.matrix(pv.rewards[:, 1]).T
        clean_run = process_prime.tri_objective_value_iteration(gamma, 0.0, 1.0)
        u_lower = np.matrix([[ 2429.90331099], [ 2413.67356102], [ 2406.63520053], [ 2383.81956538], [ 2381.30524477]])
        u_expected = np.matrix([[ 2853.48189502], [ 2836.1315332 ], [ 2837.35671008], [ 2815.97587736], [ 2796.41225712]])
        u_upper = np.matrix([[ 3307.28017361], [ 3294.58085881], [ 3303.04233278], [ 3271.9445682 ], [ 3241.13457013]])
        # test unclean run
        for k in range(981):
            u_upper, u_lower, u_expected, _ = \
                process_prime.tri_objective_interval_value_iteration_step(gamma, 0.0, 1.0, u_upper, u_lower, u_expected)
        # Let's make some debugging observations.
        # IVI == interval_value_iteration
        # PE == evaluate_policy
        # We can see that if we fix the action sets to singletons, IVI is the same as PE. Furthermore: convergence is
        # independent of the starting point (so, probably no inherent numerical problems).
        # So, the problem is most probably in interval_value_iteration_step, where we have a choice (strange? probably.)
        # Hypothesis 1: Premature update fucks things up. Confirmed!
        relative_error_expected = expected_pe / expected_vi

    if strange_test:
        print("Computing a very strange policy...")
        strange_weight = 0.07802986969465364
        strange_solution_l = process.tri_objective_value_iteration(gamma, 0.0, strange_weight - 0.01)
        print("1/3")
        strange_solution_o = process.tri_objective_value_iteration(gamma, 0.0, strange_weight + 0.01)
        print("2/3")
        strange_solution_e = process.tri_objective_value_iteration(gamma, 0.0, strange_weight)
        print("3/3")

    t0 = datetime.now()
    if compromise:
        print("Computing compromise solutions…")
        expected_solution = process.tri_objective_value_iteration(gamma, 0.0, 0.0)
        lower_solution = process.tri_objective_value_iteration(gamma, 0.0, 1.0)
        upper_solution = process.tri_objective_value_iteration(gamma, 1.0, 0.0)

        dt = datetime.now() - t0
        t0 = datetime.now()
        print("Done in {0} s, avg: {1} s".format(dt.total_seconds(), dt.total_seconds() / 3.0))

    if pure_pareto_optimal:
        print("Computing pure Pareto optimal solutions…")
        pure_solutions = process.pure_opt(gamma, 2)

        dt = datetime.now() - t0
        t0 = datetime.now()
        print("Done in {0} s".format(dt.total_seconds()))
        for s in range(5):
            xvals = [x.rewards[s, 0] for x in pure_solutions]
            yvals = [x.rewards[s, 1] for x in pure_solutions]
            plt.plot(xvals, yvals, label="State {0}".format(s))
        plt.legend()

    if pareto_optimal:
        print("Computing Pareto optimal value vectors…")
        pareto_solutions = process.pareto_opt(gamma, 0.01, 0.01, maxiter=6)

        dt = datetime.now() - t0
        t0 = datetime.now()
        print("Done in {0} s".format(dt.total_seconds()))

    if weight_optimal:
        print("Computing weight-optimal solutions…")
        weight_solutions, weights = process.weight_opt(gamma, 2)

        dt = datetime.now() - t0
        t0 = datetime.now()
        print("Done in {0} s".format(dt.total_seconds()))
    if benchmark:
        nsamples = 5
        print("Benchmarking...")
        state_spaces = [5, 10, 100, 1000]
        action_spaces = [1, 2, 3, 4, 5, 6, 7, 8]
        for ns in state_spaces:
            for na in action_spaces:
                print("{0} states, {1} actions, {2} vars:".format(ns, na, ns * na))
                # sample some meaningful statistics
                runtimes = [0 for sample in range(10)]
                creation_times = [0 for sample in range(10)]
                for sample in range(nsamples):
                    t1 = datetime.now()
                    p = create_random_process(ns, na)
                    dt1 = datetime.now() - t1
                    p.tri_objective_value_iteration(0.8, 0.0, 0.0)
                    dt = datetime.now() - t0
                    t0 = datetime.now()
                    runtimes[sample] = dt.total_seconds()
                    creation_times[sample] = dt1.total_seconds()
                mean = sum(runtimes) / nsamples
                stdev = np.std(runtimes)
                creation_mean = sum(creation_times) / nsamples
                creation_stdev = np.std(creation_times)
                print("mean {0}({2}) s; stdev {1}({3})".format(mean, stdev, creation_mean, creation_stdev))

        print("Done in {0} s".format(dt.total_seconds()))
    if real_world:
        print("trying a real-world example...")
        mdp = read_imdp_from_file("/home/scheftel/Dokumente/papers/MDPNuncertainty/Net/K1.mdpu")
        dt = datetime.now() - t0
        t0 = datetime.now()
        print("loading took {0} s".format(dt.total_seconds()))
        real_results = mdp.tri_objective_value_iteration(0.5, 0.3, 0.3)
        dt = datetime.now() - t0
        print("Done in {0} s".format(dt.total_seconds()))
    if dump:
        print(dump_imdp(process))

    sensible_policies = [[0, 0, 1, 3, 4], [0, 0, 2, 2, 4], [0, 0, 2, 3, 4], [0, 0, 1, 4, 4], [0, 0, 2, 4, 4]]
    tuples = [PolicyRewardTuple(5, 2) for _ in sensible_policies]
    if False:
        for i in range(len(sensible_policies)):
            tuples[i].decisions = sensible_policies[i]
            process.evaluate_policy(tuples[i], gamma)
            print("Policy {0}: {1}".format(i, sensible_policies[i]))
            print(tuples[i].rewards)
