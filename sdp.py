#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import numpy as np
import scipy.linalg as scl
from scipy.integrate import quad
import mdp
import sg
from operator import mul
from functools import reduce
from math import log, ceil
import itertools
import functools
import matplotlib.pyplot as plt

__author__ = 'Dimitri Scheftelowitsch'


def reverse_enumerate(iterable):
    """
    Enumerate over an iterable in reverse order while retaining proper indexes
    """
    return zip(reversed(range(len(iterable))), reversed(iterable))


class PHClock:
    mD_0 = np.matrix([[-1.0, 1.0], [0.0, -1.0]])
    vpi_0 = np.matrix([1.0, 0.0])

    state = vpi_0

    def __init__(self, D0, pi0):
        self.mD_0 = D0
        self.vpi_0 = pi0

    def run(self, t):
        """
        Runs the clock for t time steps
        @param t the number of clock steps to simulate
        """
        state = self.state * scl.expm(self.mD_0 * t)
        return state

    def run_from_zero(self, t):
        """
        Runs the clock for t time steps from zero
        @param t the number of clock steps to simulate
        """
        state = self.vpi_0
        return self.run(t)

    def prob(self):
        """
        Returns the firing probability
        """
        return 1.0 - np.sum(self.state)

    def prob_t(self, t):
        """
        Returns the firing probability after t time steps
        @param t the number of clock steps to simulate
        """
        matrix = self.mD_0 * t
        matrix_exponential = scl.expm(matrix)
        return 1.0 - np.sum(self.state * matrix_exponential)

    def density_t(self, t):
        d = -np.sum(self.mD_0, axis=1)
        matrix = self.mD_0 * t
        matrix_exponential = scl.expm(matrix)
        v = (self.state * matrix_exponential * d)[0, 0]
        return v

    def states(self):
        """
        Returns the number of states in the clock
        """
        return self.vpi_0.shape[1] + 1

    def uniform(self, q):
        """
        Returns a uniformized representation of the clock with a given uniformization rate
        @param q the uniformization rate
        """
        assert q >= self.rate()
        return np.eye(self.states() - 1) + self.mD_0 / q

    def full(self):
        mD = np.matrix(np.zeros((self.states(), self.states())))
        mD[0:self.states() - 1, 0:self.states() - 1] = self.mD_0
        mD[0:self.states() - 1, self.states() - 1] = -(np.sum(self.mD_0, axis=1)[:, 0])
        return mD

    def uniform_full(self, q):
        assert q >= self.rate()
        return self.full() / q + np.eye(self.states())

    def uniform_sub(self, q):
        assert q >= self.rate()
        return self.mD_0 / q + np.eye(self.states() - 1)

    def rate(self):
        return abs(self.mD_0).max()

    def transition_rate(self, s, t):
        if self.absorbing(s):
            return 0
        elif self.absorbing(t):
            return -np.sum(self.mD_0[s, :])
        else:
            return self.mD_0[s, t]

    def uprob(self, q, s, t):
        return self.uniform_full(q)[s, t]

    def uprob_old(self, q, s, t):
        if s == t and self.absorbing(s):
            return 1
        elif s == self.states() - 1:
            return 0
        elif t == self.states() - 1:
            return (-np.sum(self.mD_0[s, :])) / q
        elif s == t:
            return 1 + self.mD_0[s, t] / q
        else:
            return self.mD_0[s, t] / q

    def absorbing(self, s):
        return s == self.states() - 1

    def pi0(self, s):
        if self.absorbing(s):
            return 0
        else:
            return self.vpi_0[0, s]


class MMAPClock:
    pass


class Action:
    p_vector = np.matrix([])
    reset = []

    def __init__(self, p, r):
        self.p_vector = p
        self.reset = r


class Location:
    id = 0
    name = ""
    actions = {}
    enabled = []
    reward = 0


class StochasticDecisionProcess:
    locations = []
    clocks = []

    def location(self, name):
        return next(filter(lambda x: x.name == name, self.locations))

    def absorbing(self, s):
        for i, c in enumerate(self.clocks):
            if c.absorbing(s[i]):
                return i + 1, c
        return 0, None

    def translate(self, l, c, s):
        num_clock_states = reduce(mul, [c.states() for c in self.clocks])
        i, _ = self.absorbing(s)
        return l * (1 + len(self.clocks)) * num_clock_states + i * num_clock_states + self.translate_clock_state(s)

    def translate_clock_state(self, s):
        number = 0
        for i, clock in enumerate(self.clocks):
            number *= clock.states()
            number += s[i]
        return number

    def reverse_translate(self, n):
        num_clock_states = reduce(mul, [c.states() for c in self.clocks])
        location_clock_factor = num_clock_states
        location_clock = int(n / location_clock_factor)
        location = int(location_clock / (1 + len(self.clocks)))
        clock = location_clock % (1 + len(self.clocks))
        rest = n % location_clock_factor
        return location, clock, self.reverse_translate_clock_state(rest)

    def reverse_translate_clock_state(self, n):
        rval = np.array([0 for _ in self.clocks])
        for i, clock in reverse_enumerate(self.clocks):
            rval[i] = n % clock.states()
            n = int(n / clock.states())
        return rval

    def pomdp_representation(self):
        # state set is S \times \Sigma
        num_clock_states = reduce(mul, [c.states() for c in self.clocks])
        num_states = len(self.locations) * (1 + len(self.clocks)) * num_clock_states

        common_rate = sum([c.rate() for c in self.clocks])
        print("Common rate is {0}".format(common_rate))

        def translate(l, c, s):
            i, _ = self.absorbing(s)
            return l * (1 + len(self.clocks)) * num_clock_states + i * num_clock_states + translate_clock_state(s)

        def translate_clock_state(s):
            number = 0
            for i, clock in enumerate(self.clocks):
                number *= clock.states()
                number += s[i]
            return number

        def reverse_translate(n):
            location_clock_factor = num_clock_states
            location_clock = int(n / location_clock_factor)
            location = int(location_clock / (1 + len(self.clocks)))
            clock = location_clock % (1 + len(self.clocks))
            rest = n % location_clock_factor
            return location, clock, reverse_translate_state(rest)

        def reverse_translate_state(n):
            rval = np.array([0 for _ in self.clocks])
            for i, clock in reverse_enumerate(self.clocks):
                rval[i] = n % clock.states()
                n = int(n / clock.states())
            return rval

        def generate_states():
            # locations -> clocks -> clock state
            action_list = np.array([None for _ in range(num_states)])
            for s in range(num_states):
                l, c, st = reverse_translate(s)
                loc = self.locations[l]
                assert type(loc) is Location
                if c == 0:
                    # do stuff if no clock fires
                    p_vector = np.zeros((1, num_states))
                    p = None
                    for cl in loc.enabled:
                        clock = self.clocks[cl]
                        source_state = st[cl]
                        for next_state in range(clock.states()):
                            coord = np.array(st)
                            coord[cl] = next_state
                            pr = clock.transition_rate(source_state, next_state)
                            pr /= common_rate
                            p_vector[0, translate(l, c, coord)] += pr
                    p_vector[0, translate(l, c, st)] += 1.0
                    action_list[s] = [(loc.reward / common_rate, p_vector)]
                else:
                    c -= 1
                    ix, clk = self.absorbing(st)
                    if c in loc.actions and c == ix - 1:
                        for i, a in enumerate(loc.actions[c]):
                            p_vector = np.zeros((1, num_states))
                            for next_location, _ in enumerate(self.locations):
                                for encoded_state in range(num_clock_states):
                                    p = a.p_vector[0, next_location]
                                    next_coords = reverse_translate_state(encoded_state)

                                    # check if the number of states has changed
                                    changed_clocks = np.nonzero(next_coords != st)
                                    for cl, _ in enumerate(self.clocks): # change these lines if you work with MAPs
                                        if self.clocks[cl].absorbing(st[cl]) or (cl in a.reset):
                                            p *= self.clocks[cl].pi0(next_coords[cl])
                                        elif (cl != c) and (not (cl in a.reset)) and st[cl] != next_coords[cl]:
                                            p = 0
                                        #else:
                                        #    p = 0
                                    p_vector[0, translate(next_location, 0, next_coords)] = p
                            act_to_append = (loc.reward / common_rate, p_vector)
                            if action_list[s] is None:
                                action_list[s] = [act_to_append]
                            else:
                                action_list[s].append(act_to_append)
                if action_list[s] is None:
                    action_list[s] = []
            return action_list

        process = mdp.SingleObjectiveMarkovDecisionProcess(generate_states())

        return process

    def t_mdp_represenatation(self, epsilon, gamma):
        for c in self.clocks:
            assert type(c) is PHClock

        common_rate = sum([c.rate() for c in self.clocks])

        def log_lambda_fraction(clock):
            eigenvalues = np.linalg.eigvals(clock.uniform_full(common_rate))
            absolute_eigenvalues = [np.abs(x) for x in eigenvalues]
            l1 = max(absolute_eigenvalues)
            l2 = max(filter(lambda x: x != l1, absolute_eigenvalues))
            return log(l2) - log(l1)

        num_clocks = len(self.clocks)
        max_lambda_fraction = max([log_lambda_fraction(c) for c in self.clocks])
        max_reward = max(l.reward for l in self.locations)
        mu = 1.0/64  # let's think that this constant is somewhere around one. otherwise, in can be experimentally found out.
        fraction = (log(epsilon) - log(max_reward) + 2 * log(1 - gamma) - log(mu)) / max_lambda_fraction

        def uf_non_absorption(clock, time):
            return np.sum(clock.vpi_0 * clock.uniform_sub(common_rate) ** time)

        def uf_delta_non_absorption(clock, time):
            return uf_non_absorption(clock, time + 1) / uf_non_absorption(clock, time)

        def empirical_time(clock):
            # geometric search
            t = 1
            prob = 1
            while prob > (1 - gamma) ** 2 * epsilon / max_reward:
                t *= 2
                prob = uf_non_absorption(clock, t)
            return t

        max_time = int(ceil(fraction))

        max_time = max([empirical_time(clock) for clock in self.clocks])

        big_t = max_time ** num_clocks

        num_states = len(self.locations) * (1 + len(self.clocks)) * (max_time ** num_clocks)

        def decode_time(times):
            ts = np.array([0 for _ in self.clocks])
            for i, _ in reverse_enumerate(self.clocks):
                ts[i] = times % max_time
                times = int(times / max_time)
            return ts

        def encode_time(ts):
            n = 0
            for i, _ in enumerate(self.clocks):
                n *= max_time
                n += ts[i]
            return n

        def decode_state(s):
            times = s % big_t
            rest = int(s / big_t)
            clock = rest % (1 + len(self.clocks))
            location = int(rest / (1 + len(self.clocks)))
            return location, clock, decode_time(times)

        def encode_state(location, clock, time):
            return (location * (1 + len(self.clocks)) + clock) * big_t + time

        def absorption_prob(clock_id, location, times):
            clock_specific_prob = 1 - uf_delta_non_absorption(self.clocks[clock_id], times[clock_id] - 1)
            rest = 0
            rest_set = [x for x in location.enabled if x != clock_id]
            if len(rest_set) != 0:
                for subset_length in range(len(rest_set)):
                    for subset in itertools.combinations(rest_set, subset_length):
                        complement = filter(lambda x: not(x in subset), location.enabled)
                        non_absorption_events = [uf_delta_non_absorption(self.clocks[other_clock_id],
                                                                         times[other_clock_id] - 1)
                                                 for other_clock_id in complement]
                        non_absorption_prob = functools.reduce(mul, non_absorption_events, 1.0)
                        absorption_events = [1 - uf_delta_non_absorption(self.clocks[other_clock_id],
                                                                         times[other_clock_id] - 1)
                                             for other_clock_id in subset]
                        absorption_prob = functools.reduce(mul, absorption_events, 1.0)
                        weight = sum(absorption_events)
                        if abs(weight) < 1e-6:
                            rest += clock_specific_prob * non_absorption_prob
                        else:
                            rest += clock_specific_prob / weight * absorption_prob * non_absorption_prob
            else:
                rest = clock_specific_prob
            return rest

        action_lists = np.array([None for _ in range(num_states)])

        print("Number of states in the MDP model: {0}, maximum simulation time: {1}".format(num_states, max_time))

        for s1 in range(num_states):
            l1, c1, t1 = decode_state(s1)
            reward = self.locations[l1].reward / common_rate
            active_clocks = self.locations[l1].enabled

            if s1 % 1000 == 0:
                print("Step {0} of {1}…".format(s1, num_states))

            def clock_advances(clock_id):
                if clock_id in active_clocks:
                    return t2[clock_id] == min(max_time - 1, t1[clock_id] + 1)
                else:
                    return t2[clock_id] == t1[clock_id]

            def clock_advances_with_action(clock_id, firing_clock_id, action):
                if clock_id in active_clocks:
                    if clock_id in action.reset or clock_id == firing_clock_id:
                        return t2[i] == min(1, max_time - 1)
                    else:
                        return t2[i] == min(t1[i] + 1, max_time - 1)
                else:
                    return t2[i] == t1[i]

            def next_actionless_clock_value(clock_id):
                if clock_id in active_clocks:
                    return min(max_time - 1, t1[clock_id] + 1)
                else:
                    return t1[clock_id]

            def next_clock_value_and_prob(clock_id, action, next_location_id):
                next_active_clocks = self.locations[next_location_id].enabled
                value = t1[clock_id]
                if clock_id in action.reset:
                    value = 0
                if clock_id in next_active_clocks:
                    p = uf_delta_non_absorption(clock_id, value)
                    return min(value + 1, max_time - 1), p
                else:
                    return value, 1

            if c1 == 0:
                t2 = np.array([next_actionless_clock_value(i) for i, _ in enumerate(self.clocks)])
                pv = np.zeros((1, num_states))
                # first, a probability for no absorption
                prob = functools.reduce(mul, [uf_delta_non_absorption(self.clocks[i], t1[i])
                                              for i in active_clocks])
                t2_encoded = encode_time(t2)
                pv[0, encode_state(l1, 0, t2_encoded)] = prob
                # then, probabilities for absorption
                for clock_id in active_clocks:
                    pv[0, encode_state(l1, clock_id + 1, t2_encoded)] = \
                        absorption_prob(clock_id, self.locations[l1], t2)
                action_lists[s1] = [(reward, pv)]
            else:
                firing_clock_id = c1 - 1
                action_lists[s1] = list()
                for action_id, action in enumerate(self.locations[l1].actions[firing_clock_id]):
                    pv = np.zeros((1, num_states))
                    for location_id, location in enumerate(self.locations):
                        # first, do the non-absorbing stuff
                        values_and_probs = [next_clock_value_and_prob(i, action, location_id)
                                            for i, _ in enumerate(self.clocks)]
                        t2 = np.array([x[0] for x in values_and_probs])
                        prob = action.p_vector[location_id] * functools.reduce(mul, [x[1] for x in values_and_probs])
                        pv[0, encode_state(location_id, 0, encode_time(t2))] = prob
                        # then, compute absorption probabilities
                        for clock_id in location.enabled:
                            # now compute the probability. It is somewhat non-trivial
                            pv[0, encode_state(location_id, clock_id + 1, encode_time(t2))] = \
                                absorption_prob(clock_id, location, t2)
                    action_lists[s1].append((reward, pv))

        return mdp.SingleObjectiveMarkovDecisionProcess(action_lists)


    def game_representation(self, gamma):
        """
        Computes a game representation.
        """
        clock_sets = [None for _ in self.clocks]
        power_sets = [None for _ in self.clocks]

        for i, c in enumerate(self.clocks):
            clock_set = [x for x in range(c.states()) if not(c.absorbing(x))]
            clock_sets[i] = frozenset(clock_set)
            psets = itertools.chain.from_iterable(itertools.combinations(clock_set, r) for r in range(1, c.states()))
            power_sets[i] = list(map(frozenset, psets))
            print("{1} {2} {0}".format(list(power_sets[i]), str(i), clock_sets[i]))

        clock_space = list(map(list, itertools.product(*clock_sets)))
        clock_subsets = list(itertools.product(*power_sets))

        print("clock space: {0}".format(clock_space))
        print("0 Enabled: {0}".format([x.enabled for x in self.locations]))

        p1_states = list(itertools.product(range(len(self.locations)), range(len(self.clocks)), range(len(clock_subsets))))
        p2_states = list(itertools.product(range(len(self.locations)), range(len(clock_subsets))))
        pS_states = list(itertools.product(range(len(self.locations)), range(len(clock_space))))

        def decode_state_from_number(n):
            modulus = n % 3
            rest = int(n / 3)
            if modulus == 0:
                # p1
                e = rest % len(clock_subsets)
                ls = int(rest / len(clock_subsets))
                c = ls % len(self.clocks)
                l = int(ls / len(self.clocks))
                return l, c, e
            elif modulus == 1:
                e = rest % len(clock_subsets)
                l = int(rest / len(clock_subsets))
                return l, e
            elif modulus == 2:
                s = rest % len(clock_space)
                l = int(rest / len(clock_space))
                return l, s

        def encode_p1_state(l, c, e):
            return ((l * len(self.clocks) + c) * len(clock_subsets) + e) * 4

        def encode_p2_state(l, e):
            return (l * len(clock_subsets) + e) * 4 + 1

        def encode_pS_state(l, s):
            return (l * len(clock_space) + s) * 4 + 2

        def decode_state_set(n):
            #  interpret n as coordinates
            coords = np.array([0 for _ in self.clocks])
            for i, c in reverse_enumerate(self.clocks):
                coord = n % len(list(power_sets[i]))
                coords[i] = coord
                n = int(n / len(list(power_sets[i])))
            return coords

        def coords_to_set(coords):
            return [power_sets[i][coords[i]] for i in range(len(self.clocks))]

        def encode_state_set_for_clock(state_set, clock_index):
            for i, t in enumerate(power_sets[clock_index]):
                if t == state_set:
                    return i
            print("failed conversion: {0}".format(state_set))
            raise ValueError
            return -1

        def sets_to_coords(sets):
            coords = [encode_state_set_for_clock(s, i) for i, s in enumerate(sets)]
            return coords

        def coords_to_index(coords):
            n = 0
            for i, c in enumerate(self.clocks):
                n *= len(list(power_sets[i]))
                n += coords[i]
            return n

        def clock_subsets_to_set(sets):
            #  num_states = reduce(mul, map(len, sets))
            sets = list(sets)
            temp_sets = [np.array([x for _ in self.clocks]) for x in sets[0]]
            for i in range(len(self.clocks) - 1):
                temp_sets_1 = []
                for coord in temp_sets:
                    for next_coord in sets[i + 1]:
                        new_coord = np.array(coord)
                        new_coord[i + 1] = next_coord
                        temp_sets_1.append(new_coord)
                temp_sets = temp_sets_1
            return temp_sets

        def reachable_clock_states(clk, s):
            def one_step_reachable(s):
                return set(filter(lambda t: clk.transition_rate(s, t) != 0 and not(clk.absorbing(t)), range(clk.states())))
            pool = [s]
            change = True
            while change:
                next_pool = set(pool)
                for q in pool:
                    next_pool = set.union(next_pool, one_step_reachable(q))
                if pool == next_pool:
                    change = False
                else:
                    pool = next_pool
            return frozenset(pool)

        def extend_reachable_states(location_index, clock_index, action, previous_set):
            c = self.clocks[clock_index]
            r = action.reset

            if clock_index in r:
                return frozenset(filter(lambda i: c.pi0(i) != 0, range(c.states())))
            else:
                return previous_set

        def actionless_reachable_states(location_index, clock_index, fired_clock, s):
            c = self.clocks[clock_index]

            if fired_clock == clock_index:
                return frozenset(filter(lambda i: c.pi0(i) != 0, range(c.states)))
            elif clock_index in self.locations[location_index].enabled:
                temp_states = reachable_clock_states(self.clocks[clock_index], s)
                return frozenset(filter(lambda s: not(c.absorbing(s)), temp_states))
            else:
                return frozenset([s])

        num_of_states = 4 * len(list(p1_states))

        actions = np.array([None for _ in range(num_of_states)])

        for (l, c, e) in p1_states:
            state_index = encode_p1_state(l, c, e)
            if c in self.locations[l].actions:
                for a in self.locations[l].actions[c]:
                    e_as_sets = coords_to_set(decode_state_set(e))
                    extended_sets = [extend_reachable_states(l, i, a, prev) for i, prev in enumerate(e_as_sets)]
                    # new_e = list(itertools.product(*extended_sets))
                    new_e_as_number = coords_to_index(sets_to_coords(extended_sets))

                    p_vector = np.matrix(np.zeros((1, num_of_states)))
                    for next_location, _ in enumerate(self.locations):
                        p_vector[0, encode_p2_state(next_location, new_e_as_number)] = a.p_vector[0, next_location]
                    if actions[state_index] is None:
                        actions[state_index] = [[(self.locations[l].reward, p_vector)]]
                    else:
                        actions[state_index].append([(self.locations[l].reward, p_vector)])
            if actions[state_index] is None:
                actions[state_index] = []

        print("1 Enabled: {0}".format([x.enabled for x in self.locations]))

        for (l, e) in p2_states:
            state_index = encode_p2_state(l, e)
            e_set = clock_subsets_to_set(coords_to_set(decode_state_set(e)))
            for s in e_set:
                number = -1
                for j, sets in enumerate(clock_space):
                    if all(sets[i] == s[i] for i in range(len(self.clocks))):
                        number = j
                        break

                if number == -1:
                    raise ValueError
                # number = self.translate_clock_state(s)
                p_vector = np.matrix(np.zeros((1, num_of_states)))
                p_vector[0, encode_pS_state(l, number)] = 1.0
                if actions[state_index] is None:
                    actions[state_index] = [[(self.locations[l].reward, p_vector)]]
                else:
                    actions[state_index][0].append((self.locations[l].reward, [p_vector]))
            if actions[state_index] is None:
                actions[state_index] = []

        print("2 Enabled: {0}".format([x.enabled for x in self.locations]))

        for (l, s) in pS_states:
            print("2.{1}.{2} Enabled: {0}".format([x.enabled for x in self.locations], l, s))
            state_index = encode_pS_state(l, s)
            #  do the actual probabilistic part here…
            p_vector = np.matrix(np.zeros((1, num_of_states)))
            coords = clock_space[s]  # self.reverse_translate_clock_state(s)
            valid = all(not(clk.absorbing(coords[i])) for i, clk in enumerate(self.clocks))
            # print("{0}, {1}: coords {2}, valid: {3}".format(l, s, coords, valid))

            if valid:
                t_l_s_clk = np.array([0.0 for _ in self.clocks])
                q_l_s_clk = np.array([0.0 for _ in self.clocks])
                for i in self.locations[l].enabled:
                    clk = self.clocks[i]

                    def p_c(t):
                        p = 1
                        for j in self.locations[l].enabled:
                            c = self.clocks[j]
                            c.state = np.matrix(np.zeros((1, c.states() - 1)))
                            if not(c.absorbing(coords[j])):
                                c.state[0, coords[j]] = 1.0
                            if j == i:
                                p *= c.density_t(t)
                            else:
                                p *= (1.0 - c.prob_t(t))
                        return p
                    clk.state = np.matrix(np.zeros((1, clk.states() - 1)))
                    if not(clk.absorbing(coords[i])):
                        clk.state[0, coords[i]] = 1.0
                    t_l_s_clk[i] = -np.sum(clk.state * scl.inv(clk.mD_0))
                    upper_limit = 200  # change to np.inf
                    result, error = quad(p_c, 0, upper_limit)
                    q_l_s_clk[i] = result
                    # print("q_{0}_{1}_{2} = {3} ± {4}".format(l, s, i, q_l_s_clk[i], error))
                    if q_l_s_clk[i] == 0:
                        # make a fancy plot
                        XXX = np.arange(0, upper_limit, 0.1)
                        YYY = np.array(list(map(p_c, XXX)))
                        plt.plot(XXX, YYY)
                        plt.show()
                        q_l_s_clk[i] = np.sum(YYY) * 0.1
                        print("numerical value: {0}".format(q_l_s_clk[i]))

                t_l_s = np.sum(t_l_s_clk * q_l_s_clk)
                print("t_{0}_{1} = <{3}, {4}> = {2}".format(l, s, t_l_s, t_l_s_clk, q_l_s_clk))

                p_vector[0, state_index] = 1.0 - 1.0 / (t_l_s - 2.0)
                for i, clk in enumerate(self.clocks):
                    all_reachable_states = [actionless_reachable_states(l, i, clk, coords[i]) for i in range(len(self.clocks))]
                    p_vector[0, encode_p1_state(l, i, coords_to_index(sets_to_coords(all_reachable_states)))] = q_l_s_clk[i] / (t_l_s - 2.0)

                actions[state_index] = [[(self.locations[l].reward, p_vector)]]
            else:
                actions[state_index] = []

        zero_p_vector = np.matrix(np.zeros((1, num_of_states)))
        for state_index in range(num_of_states):
            if actions[state_index] is None:
                actions[state_index] = []

        # process
        print("3 Enabled: {0}".format([x.enabled for x in self.locations]))

        game = sg.StochasticGame(actions)
        v, p = game.value_iteration(gamma, 1e-5)
        v /= 10
        print("======== USER ACTIONS ========")
        print("Location & Clock & Possible states & Action & Value \\")
        for (l, c, e) in p1_states:
            if c in self.locations[l].enabled:
                state_index = encode_p1_state(l, c, e)
                e_as_sets = coords_to_set(decode_state_set(e))
                print("{0} & {1} & {2} & {3} & {4} \\\\".format(l, c, e_as_sets, p[state_index, 0], v[state_index]))
        print("======== ADVERSARY ACTIONS ========")
        print("Location & Possible states & Action & Value \\")
        for (l, e) in p2_states:
            state_index = encode_p2_state(l, e)
            e_as_sets = coords_to_set(decode_state_set(e))
            action = p[state_index, 1]
            uncertainty_set = clock_subsets_to_set(coords_to_set(decode_state_set(e)))
            print("{0} & {1} & {2} & {3} \\\\".format(l, e_as_sets, uncertainty_set[action], v[state_index]))
        import pdb
        pdb.pm()
        return sg.StochasticGame(actions)