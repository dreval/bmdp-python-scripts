#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sdp
import numpy as np
import scipy.linalg as scp
import matplotlib.pyplot as plt

__author__ = 'Dimitri Scheftelowitsch'

D_0 = np.matrix([[-2,  1,   1],
                 [1,  -2, 0.5],
                 [0,   1,  -2]])
pi_0 = np.matrix([1.0, 0.0, 0.0])

m1 = 0.5
m2 = 4.0

D_0r = np.matrix([[-m1, m1],
                  [0.0, -m2]], dtype=np.float128)
pi_0r = np.matrix([1.0, 0.0])

lb, ub = 0, 20
X = np.arange(lb, ub, 0.01)


def evaluate(t):
    return 1 - np.sum(pi_0r * scp.expm((D_0r * t).astype(np.float32)))

values = np.array([evaluate(t) for t in X])

d = - np.sum(D_0, axis=1)


def pdf(t):
    v = (pi_0 * scp.expm((D_0 * t).astype(np.float128)) * d)[0, 0]
    return v

pdfs = np.array([pdf(t) for t in X])

plt.plot(X, values, 'b', X, pdfs, 'r')
plt.show()

# alright, PHDs work fine. Now for some BDSM...

clock = sdp.PHClock(D_0, pi_0)

act0 = sdp.Action(np.matrix([1.0]), [])
loc0 = sdp.Location()

loc0.actions[0] = [act0]
loc0.enabled = [0]
loc0.reward = 3
loc0.name = "init"

sp = sdp.StochasticDecisionProcess()
sp.clocks = [clock]
sp.locations = [loc0]

proc = sp.pomdp_representation()

for s, actions in enumerate(proc.actions):
    print("reachable from {0}: {1}".format(s, proc.reachable(s)))
