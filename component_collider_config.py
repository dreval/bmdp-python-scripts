# -*- author: Dimitri Scheftelowitsch -*-
# -*- coding:utf-8 -*-

"""
A config file for collider

Generates a sequence of CMDPs and BMDPs and runs some not very sophisticated analysis on
them.
"""
import logging
import numpy as np
import pandas as pd
import matplotlib
from typing import Any, Dict, Mapping
import os
import collider

matplotlib.use('PDF')
from matplotlib import pyplot as plt


if __name__ == "__main__":
    experiment_name = "composed_components"

    # create directory for intermediate results
    directory = "{}_intermediate".format(experiment_name)
    if not os.path.exists(directory):
        os.mkdir(directory)

    printer = logging.getLogger("collider")
    printer.setLevel(logging.DEBUG)
    LOG_FORMAT = "%(asctime)s %(levelname)s: [%(module)s:%(lineno)d:%(funcName)s] %(message)s"
    logging.basicConfig(format=LOG_FORMAT)

    values = {
        "components": [2, 3],
        "phases": [2, 4]
    }

    generate_stage = collider.ExecutableFileOutputStage(
        experiment_name,
        'python3',
        list(values.keys()),
        ['/home/scheftel/projects/bmdp-in-python/component_generator.py',
         '--components={components}',
         '--phases={phases}']
    )
    generate_stage.name = 'generate'
    exe = '/home/scheftel/projects/BMDP/BMDPanalysis'

    def gamma_from_stats(stats: Dict[str, Any]) -> float:
        generate_stats = stats['generate']['output'].decode('utf-8')
        lines = generate_stats.split('\n')
        last_line = lines[-2]
        gamma_and_text = last_line.split(' ')
        gamma_string = gamma_and_text[0]
        printer.debug("\gamma = float({0}) = {1}".format(gamma_string, float(gamma_string)))
        return gamma_string

    class CMDPStage(collider.ExecutableFileOutputStage):
        def __init__(self):
            collider.ExecutableFileOutputStage.__init__(self, experiment_name, exe, sorted(values.keys()),
                                                        ['component_model_c{components}_r1_s{phases}.cmdp', '902',
                                                         '{gamma}', '1'])
            self.name = 'cmdp_stage'

        def execute(self, value: Mapping[int, Any], stats: Dict[str, Any]):
            # extract gamma
            self.patterns[-2] = gamma_from_stats(stats)
            return collider.ExecutableFileOutputStage.execute(self, value, stats)

    class BMDPStage(collider.ExecutableFileOutputStage):
        def __init__(self):
            collider.ExecutableFileOutputStage.__init__(self, experiment_name, exe, sorted(values.keys()),
                                                        ['component_model_c{components}_r1_s{phases}', '805',
                                                         '{gamma}'])
            self.name = 'bmdp_stage'

        def execute(self, value: Mapping[int, Any], stats: Dict[str, Any]):
            # extract gamma
            self.patterns[-1] = gamma_from_stats(stats)
            return collider.ExecutableFileOutputStage.execute(self, value, stats)

    class PainterStage(collider.Stage):
        def __init__(self):
            collider.Stage.__init__(self, "draw")

        def execute(self, value: Mapping[int, Any], stats: Dict[str, Any]) -> Dict[str, Any]:
            value_keys = sorted(values.keys())
            value_dict = {}
            for i, a in enumerate(value_keys):
                value_dict[a] = value[i]

            phases = value_dict['phases']
            components = value_dict['components']
            printer.debug("Assuming that {0} means {1} phases and {2} components".format(value, phases, components))
            # open file with results and paint them nicely
            # first, open the BMDP result file
            # current_directory = directory_pattern.format(experiment_name, phases, components)
            basename = "component_model_c{0}_r1_s{1}"

            directory = "{1}_intermediate/{0}".format("_".join([repr(v) for v in value]), experiment_name)
            current_basename = basename.format(components, phases)
            has_bmdp = True
            try:
                bmdp_value_file = "{0}/{1}_805.pareto_vals".format(directory, current_basename)
                pareto_vals = np.ndfromtxt(bmdp_value_file).T
                axis_descriptions = ['min', 'avg', 'max']

                for axis in [(0, 1), (1, 2), (0, 2)]:
                    axis_description = "{0}{1}".format(axis_descriptions[axis[0]], axis_descriptions[axis[1]])

                    plt.clf()
                    plt.plot(pareto_vals[axis[0]+1], pareto_vals[axis[1]+1], "b.", label="Pareto frontier")
                    # printer.debug("CMDP result for {2}: {0}, {1}".format(cmdp_x, cmdp_y, axis_description))
                    #plt.plot([cmdp_x], [cmdp_y],
                    #         color="orange", marker=".", label="CMDP result")
                    plt.xlabel(axis_descriptions[axis[0]])
                    plt.ylabel(axis_descriptions[axis[1]])
                    plt.savefig("{0}/{1}_{2}.pdf".format(directory, current_basename, axis_description))
            except OSError:
                printer.warning("Warning: Could not find BMDP results. Proceeding without them.")

            cmdp_array = np.zeros((3, 4))
            cmdp_value_file = "{0}/{1}.cmdp_vals".format(directory, current_basename)
            cmdp_vals = np.ndfromtxt(cmdp_value_file)
            for scenario in range(3):
                cmdp_array[scenario, 0] = cmdp_vals[0, scenario]
                cmdp_scenario_file = "{0}/{1}_scenario_{2}.cmdp_vals".format(directory, current_basename, scenario)
                cmdp_scenario_vals = np.ndfromtxt(cmdp_scenario_file)
                for k in range(3):
                    cmdp_array[k, scenario + 1] = cmdp_scenario_vals[0, k]

            table_string = ""
            scenario_descriptions = ["fast degrading, slow maintenance", "average", "slow degrading, fast maintenance"]
            for scenario, text in enumerate(scenario_descriptions):
                table_string += "{0} &\n {1} \\\\\n".format(text, " & ".join(map("\\num{{{0}}}".format, cmdp_array[scenario, :])))

            with open("{0}/{1}_table.tex".format(directory, current_basename), "w+") as fd:
                print(table_string, file=fd)

            return stats

    stages = [generate_stage,
              CMDPStage(),
              BMDPStage(),
              PainterStage()]

    result_log = collider.ResultLog(experiment_name, values)

    stage_groups = [[stage.name] for stage in stages]
    if_needed = collider.RerunIfNeeded(result_log)
    collider.run_experiments(values, stages, result_log, collider.AlwaysRerun(), stage_groups)
