#!/usr/bin/env bash

# define variables
OUTPUT_DIR=testcases
BINDIR=/home/scheftel/projects/BMDP
STATISTICS_FILE=statistics
TEMP_FILE=temp

# some constants
COMPUTE_FRONT_OPTION=805
GAMMA="0.9"

# create directory
mkdir -p ${OUTPUT_DIR}

# create output file
echo "Size; Width; Depth; Time; Policies" > ${OUTPUT_DIR}/${STATISTICS_FILE}

width_list="5 6 7 8 9 10 12 14 16 18 20"
depth_list="5 6 7 8 9 10 12 14 16 18 20"
testcase_list="1 2 3 4"

for width in $width_list
do
   for depth in $depth_list
   do
      echo "Model size: ${width}x${depth}"
      for testcase in $testcase_list
      do
         basename=testcase_grid_w${width}_d${depth}_${testcase}
         if [ ! -f "${OUTPUT_DIR}/${basename}.mdpu" ]; then
            ./imdp_generator.py --grid -w ${width} -d ${depth} > ${OUTPUT_DIR}/${basename}.mdpu
         fi
         size=$((${width}*${depth}))
         time_format="${size}; ${width}; ${depth}; %e; "
         /usr/bin/time --format="${time_format}" --output=${OUTPUT_DIR}/${TEMP_FILE} $BINDIR/BMDPanalysis ${OUTPUT_DIR}/${basename} ${COMPUTE_FRONT_OPTION} ${GAMMA}
         policies=$((`wc -l ${OUTPUT_DIR}/${basename}.pareto_stat | cut -d " " -f 1 -` - 2))
         time_output=`cat ${OUTPUT_DIR}/${TEMP_FILE}`
         echo $time_output $policies >> $OUTPUT_DIR/$STATISTICS_FILE
      done
   done
done
