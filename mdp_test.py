#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import numpy as np
import mdp

__author__ = 'Dimitri Scheftelowitsch'

EPSILON = 1e-5

state1 = [(2.0, np.matrix([0.5, 0.5])),
          (3.0, np.matrix([0.8, 0.2]))]
state2 = [(4.0, np.matrix([0.2, 0.8])),
          (6.0, np.matrix([0.5, 0.5]))]

p = mdp.SingleObjectiveMarkovDecisionProcess([state1, state2])
p.decision_table = np.array([0, 1])

values = p.policy_evaluation(0.5, EPSILON)

r = p.value_iteration(0.5, EPSILON)