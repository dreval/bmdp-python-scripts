import numpy as np
from typing import List
from imdp import sparse_matrix_representation


class ConcurrentMarkovDecisionProcess:
    def __init__(self, matrices: List[List[np.matrix]], rewards: List[np.array], iniv: np.array, weights: np.array):
        self.matrices = matrices
        self.rewards = rewards
        self.iniv = iniv
        self.weights = weights

    def dump_as_string(self):
        scenarios = len(self.matrices)
        scenario_matrix = np.matrix([[scenarios]])
        # scenarios, K |A| transition matrices, K reward vectors, one initial vector, one weight vector
        num_matrices = 1 + scenarios * (len(self.matrices[0]) + 1) + 2
        str = "# CMDP with {0} scenarios\n{2}\n{1}".format(scenarios, sparse_matrix_representation(scenario_matrix), num_matrices)
        for k in range(scenarios):
            str += "".join([sparse_matrix_representation(m) for m in self.matrices[k]])
            str += sparse_matrix_representation(np.matrix(np.diag(self.rewards[k])))
        str += sparse_matrix_representation(np.matrix(np.diag(self.iniv)))
        str += sparse_matrix_representation(np.matrix(np.diag(self.weights)))

        return str


def random_transition_matrix(order: int, deterministic=False) -> np.matrix:
    if deterministic:
        m = np.identity(order)
        np.random.shuffle(m)
        m = np.matrix(m)
    else:
        m = np.matrix(np.random.uniform(0, 1, (order, order)))
        m = m / np.sum(m, 1)
    return m


def generate_random_cmdp(scenarios: int, states: int, actions: int, deterministic: bool) \
        -> ConcurrentMarkovDecisionProcess:
    matrices = [[random_transition_matrix(states, deterministic) for _ in range(actions)] for _ in range(scenarios)]
    rewards = [np.random.uniform(0, 10, states) for _ in range(scenarios)]
    iniv = np.random.uniform(0, 1, states)
    iniv = iniv / np.sum(iniv)
    weights = np.random.uniform(0, 1, scenarios)
    weights = weights / np.sum(weights)
    return ConcurrentMarkovDecisionProcess(matrices, rewards, iniv, weights)


if __name__ == "__main__":
    reward_1 = np.array([0, 3])
    reward_2 = np.array([3, 9])
    matrix_a_1 = matrix_b_2 = np.matrix([[1, 0],
                                         [1, 0]])
    matrix_a_2 = matrix_b_1 = np.matrix([[0, 1],
                                         [0, 1]])

    weights = np.array([0.7, 0.3])
    iniv = np.array([2.0/3.0, 1.0/3.0])

    cmdp = ConcurrentMarkovDecisionProcess([[matrix_a_1, matrix_b_1], [matrix_a_2, matrix_b_2]], [reward_1, reward_2],
                                           iniv, weights)
    print(cmdp.dump_as_string())