#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Provides classes used for multi-criterial BMDP optimization.

The functionality is documented in Scheftelowitsch, Buchholz:
Multi-Criteria Approaches to Markov Decision Processes
with Uncertain Transition Parameters

"""

from mdp import *
import numpy as np
import scipy.sparse as sp
# from collections import Iterator
import scipy.special
from cvxopt import matrix, solvers
# from joblib import Parallel, delayed
# import math
# import pdb
import random
import itertools

__author__ = 'Dimitri Scheftelowitsch'


sparse = False
if sparse:
    from scipy.sparse import csr_matrix as nmatrix
else:
    from numpy import matrix as nmatrix


def permutation_indices(data):
    """
    Returns the permutation of the data from a sorted container
    """
    def get_item(key):
        # this is an evil hack
        return data[key, 0]

    def get_item_1d(key):
        return data[key]

    length = data.shape[0]
    k = get_item_1d
    if len(data.shape) == 2:
        k = get_item

    return sorted(range(length), key=k)


def max_error_factor(k, gamma):
    """
    Computes the maximal error factor for a given iteration count and
    discount factor

    @param k The number of iterations
    @param gamma The discount factor
    @type k int
    @type gamma float
    """
    return (gamma ** k) / (1.0 - gamma ** k)


class IntervalAction:
    """
    A class for actions in ``classic'' BMDPs, as defined by Givan et al., in
    ``Bounded-parameter Markov decision processes''

    """
    reward = (0.0, 0.0)
    transitions = (nmatrix([]), nmatrix([]))
    expected_reward = 0.0
    expected_case = nmatrix([])

    def __init__(self, reward, transitions, expected_case, expected_reward):
        self.reward = reward
        self.transitions = (nmatrix(transitions[0]), nmatrix(transitions[1]))
        self.expected_case = expected_case
        self.expected_reward = expected_reward

    def build_p(self, order):
        """
        Computes an order-sensitive transition vector that pushes the transition
        probability mass towards a preferred order of states.

        @param order The preferred order of states to switch to
        """
        # assert isinstance(order, Iterator)
        # take the lower bound matrix
        p = None
        if sparse:
            p = self.transitions[0].tolil()
        else:
            p = nmatrix(self.transitions[0])
        total_p = p.sum()
        deficit = 1.0 - total_p
        small_value = 1e-6
        while deficit > small_value:
            index = next(order)
            p_value = p[0, index]
            max_value = self.transitions[1][0, index]
            if p_value < max_value:
                addition = min(max_value - p_value, deficit)
                p[0, index] = p_value + addition
                deficit -= addition
        if sparse:
            return p.tocsr()
        else:
            return p

    def upper_bound(self, target):
        reward = self.reward[1]  # upper bound of the reward, no exceptions
        indices = reversed(permutation_indices(target))
        return reward, self.build_p(indices)

    def lower_bound(self, target):
        reward = self.reward[0]  # lower bound of the reward, no exceptions
        indices = iter(permutation_indices(target))
        return reward, self.build_p(indices)

    def fast_lower_bound(self, order):
        return self.reward[0], self.build_p(iter(order))

    def fast_upper_bound(self, order):
        return self.reward[1], self.build_p(reversed(order))


class IntervalMarkovDecisionProcess(MarkovDecisionProcess):
    """
    A class for ``classic'' BMDPs, as defined by Givan et al., in
    ``Bounded-parameter Markov decision processes''
    """

    def interval_value_iteration_step(self, gamma, upper_bound, lower_bound):
        """
        Performs one step from the IVI procedure as defined by Givan, Leach and
        Dean

        @param gamma The discount factor
        @param upper_bound The current value function of the optimistic policy
        @param lower_bound The current value function of the pessimistic policy
        @return A tuple consisting of the new upper bound, the new lower bound,
        and the respective policies
        """
        # this is somewhat counter-intuitive, but self.actions is a list of
        # lists of actions
        state_list = range(len(self.actions))
        optimistic_policy = [0 for _ in range(len(self.actions))]
        pessimistic_policy = [0 for _ in range(len(self.actions))]
        upper_order = permutation_indices(upper_bound)
        lower_order = permutation_indices(lower_bound)
        for state in state_list:
            lower = 0.0  # lower_bound[state, 0]
            upper = 0.0  # upper_bound[state, 0]
            action_list = range(len(self.actions[state]))
            for action_idx in action_list:
                action = self.actions[state][action_idx]
                upper_reward, upper_p = action.fast_upper_bound(upper_order)
                lower_reward, lower_p = action.fast_lower_bound(lower_order)
                new_lower = lower_reward + (gamma * lower_p * lower_bound)[0, 0]
                new_upper = upper_reward + (gamma * upper_p * upper_bound)[0, 0]
                if new_lower > lower:
                    pessimistic_policy[state] = action_idx
                if new_upper > upper:
                    optimistic_policy[state] = action_idx
                lower = max(lower, new_lower)
                upper = max(upper, new_upper)
            lower_bound[state, 0] = lower
            upper_bound[state, 0] = upper
        return upper_bound, lower_bound, optimistic_policy, pessimistic_policy

    def value_iteration_step(self, gamma, values):
        state_list = range(len(self.actions))
        policy = [0] * len(self.actions)
        new_values = nmatrix(np.zeros((len(self.actions), 1)))
        for state in state_list:
            new_values[state, 0] = -np.inf
            action_list = range(len(self.actions[state]))
            for action_idx in action_list:
                action = self.actions[state][action_idx]
                val = action.expected_reward + gamma * action.expected_case * \
                    values
                if val > values[state, 0]:
                    values[state, 0] = val
                    policy[state] = action_idx
        return (policy, new_values)

    def value_iteration(self, gamma):
        """
        Returns the result of value iteration

        @param gamma The discount factor
        @return The value and the resulting policy
        """
        values = nmatrix(np.zeros((len(self.actions), 1)))
        epsilon = 1e-6
        stop = False
        policy = None
        while not stop:
            (policy, new_values) = self.value_iteration_step(gamma, values)
            delta = np.sum(np.abs(values - new_values))
            values = new_values
            stop = delta > epsilon
        return (values, policy)

    def interval_value_iteration(self, gamma):
        """
        Returns the result of the interval value iteration procedure

        @param gamma The discount factor
        @return A tuple of the upper bound, the lower bound, the optimistic
        policy and the pessimistic policy
        """
        if gamma < 0.0 or gamma >= 1.0:
            raise ValueError(gamma,
                             "The discount factor must be strictly " +
                             "less than one and at least zero")
        # state_list = range(len(self.actions))
        lower_bound = nmatrix(np.zeros((len(self.actions), 1)))
        upper_bound = nmatrix(np.zeros((len(self.actions), 1)))
        optimistic_policy = None
        pessimistic_policy = None
        epsilon = 1e-6
        delta = 1.0
        while delta > epsilon:
            delta = 0.0
            new_upper_bound, new_lower_bound, optimistic_policy, \
                pessimistic_policy = \
                self.interval_value_iteration_step(gamma, upper_bound,
                                                   lower_bound)
            delta = max(delta, max(abs(new_upper_bound - upper_bound))[0, 0])
            delta = max(delta, max(abs(new_lower_bound - lower_bound))[0, 0])
            upper_bound = new_upper_bound
            lower_bound = new_lower_bound
        return upper_bound, lower_bound, optimistic_policy, pessimistic_policy


    def evaluate_policy(self, policy_and_reward, gamma):
        """
        Evaluates a given policy.

        @param policy_and_reward The policy-reward tuple. Its number of
        dimensions steers how much of the policy is evaluated: the first
        dimension is the expected part, the second dimension is the lower bound,
        the third dimension is the upper bound.
        @param gamma The discount factor
        @return The policy-reward tuple with complete value vectors
        """
        # assert isinstance(policy_and_reward, PolicyRewardTuple)
        state_list = range(len(self.actions))
        delta = 1.0
        epsilon = 0.00001
        # the precision can be adjusted in an appropriate manner
        k = 1
        while delta >= epsilon:
            delta = 0.0
            upper_order = lower_order = \
                permutation_indices(policy_and_reward.rewards[:, 1])
            if policy_and_reward.dimensions > 2:
                upper_order = \
                    permutation_indices(policy_and_reward.rewards[:, 2])
            for state in state_list:
                action = self.actions[state][policy_and_reward.decisions[state]]
                # assert isinstance(action, IntervalAction)
                if policy_and_reward.dimensions > 0:
                    expected = nmatrix(policy_and_reward.rewards[:, 0]).T
                    expected_reward, expected_p = \
                        action.expected_reward, action.expected_case
                    policy_and_reward.rewards[state, 0] = \
                        expected_reward + (gamma * expected_p * expected)[0, 0]
                    delta = max(delta, abs(policy_and_reward.rewards[state, 0]))

                if policy_and_reward.dimensions > 1:
                    lower = nmatrix(policy_and_reward.rewards[:, 1]).T
                    lower_reward, lower_p = action.fast_lower_bound(lower_order)
                    policy_and_reward.rewards[state, 1] = \
                        lower_reward + (gamma * lower_p * lower)[0, 0]
                    delta = max(delta, abs(policy_and_reward.rewards[state, 1]))

                if policy_and_reward.dimensions > 2:
                    upper = nmatrix(policy_and_reward.rewards[:, 2]).T
                    upper_reward, upper_p = action.fast_upper_bound(upper_order)
                    policy_and_reward.rewards[state, 2] = \
                        upper_reward + (gamma * upper_p * upper)[0, 0]
                    delta = max(delta, abs(policy_and_reward.rewards[state, 2]))
            # adjust delta with the precision scaling coefficient
            factor = max_error_factor(k, gamma)
            delta *= factor
            k += 1
        return policy_and_reward

    def pure_opt(self, gamma, dimensions):
        """
        Finds all Pareto optimal pure policies

        @param gamma The discount factor
        @param dimensions TODO not implemented properly yet
        @return A set of all evaluated Pareto optimal policy-value tuples
        """
        if not [2, 3].__contains__(dimensions):
            raise ValueError(dimensions, "dimensions can be only 2 or 3")

        assert isinstance(gamma, float)

        def insert_non_dominated(container, item):
            # assert isinstance(item, PolicyRewardTuple)
            dominated = False
            for i in container:
                # assert isinstance(i, PolicyRewardTuple)
                if item.dominates(i):
                    container.remove(i)
                elif i.dominates(item) or i.decisions == item.decisions:
                    dominated = True
            if not dominated:
                container.append(item)

            return dominated

        num_states = len(self.actions)
        upper_opt_value, lower_pes_value, upper_policy, lower_policy = \
            self.interval_value_iteration(gamma)
        expected_exp_value, expected_policy = \
            self.value_iteration(gamma)

        pi_av = PolicyRewardTuple(num_states, dimensions)
        pi_av.decisions = expected_policy
        self.evaluate_policy(pi_av, gamma)

        pi_lower = PolicyRewardTuple(num_states, dimensions)
        pi_lower.decisions = lower_policy
        self.evaluate_policy(pi_lower, gamma)

        pv_set = [pi_av, pi_lower]
        p_set = [pi_av, pi_lower]

        if dimensions == 3:
            pi_upper = PolicyRewardTuple(num_states, dimensions)
            pi_upper.decisions = upper_policy
            self.evaluate_policy(pi_upper, gamma)

            pv_set.append(pi_upper)
            p_set.append(pi_upper)

        while True:
            pv_prime_set = []
            pv_prime_prime_set = list(pv_set)
            while not len(pv_prime_prime_set) == 0:
                policy_reward = pv_prime_prime_set.pop()
                new_policy = list(policy_reward.decisions)

                upper_order = lower_order = \
                    permutation_indices(policy_reward.rewards[:, 1])
                if dimensions == 3:
                    upper_order = \
                        permutation_indices(policy_reward.rewards[:, 2])

                for state in range(num_states):
                    for action_idx in range(len(self.actions[state])):
                        action = self.actions[state][action_idx]
                        # assert isinstance(action, IntervalAction)
                        new_exp_value = action.expected_reward + \
                            (gamma * action.expected_case *
                             nmatrix(policy_reward.rewards[:, 0]).T)[0, 0]
                        exp_dominates = \
                            all(new_exp_value > policy_reward.rewards[state, 0])

                        pes_reward, pes_p = action.fast_lower_bound(lower_order)
                        new_pes_value = \
                            pes_reward + (gamma * pes_p *
                                          nmatrix(policy_reward.rewards[:, 1]).T
                                          )[0, 0]
                        pes_dominates = \
                            all(new_pes_value > policy_reward.rewards[state, 1])

                        opt_dominates = True
                        if dimensions == 3:
                            opt_reward, opt_p = \
                                action.fast_upper_bound(upper_order)
                            new_opt_value = \
                                opt_reward + (gamma * opt_p *
                                              nmatrix(policy_reward.
                                                      rewards[:, 2]).T)[0, 0]
                            opt_dominates = \
                                all(new_opt_value >
                                    policy_reward.rewards[state, 2])

                        if exp_dominates and pes_dominates and opt_dominates:
                            new_policy[state] = action_idx
                if new_policy != policy_reward.decisions:
                    new_tuple = PolicyRewardTuple(num_states, dimensions)
                    new_tuple.decisions = new_policy
                    self.evaluate_policy(new_tuple, gamma)
                    policy_reward.decisions = new_policy
                    self.evaluate_policy(policy_reward, gamma)

                    insert_non_dominated(pv_prime_set, policy_reward)
                    insert_non_dominated(pv_prime_prime_set, policy_reward)
                    p_set.append(policy_reward)
                else:
                    insert_non_dominated(pv_prime_set, policy_reward)

            pv_prime_prime_set = list(pv_prime_set)

            for pv in pv_prime_set:
                # assert isinstance(pv, PolicyRewardTuple)

                upper_order = lower_order = \
                    permutation_indices(pv.rewards[:, 1])
                if dimensions == 3:
                    upper_order = permutation_indices(pv.rewards[:, 2])

                for state in range(num_states):
                    for action_idx in range(len(self.actions[state])):
                        if action_idx != pv.decisions[state]:
                            action = self.actions[state][action_idx]
                            # assert isinstance(action, IntervalAction)
                            policy_prime = list(pv.decisions)
                            policy_prime[state] = action_idx

                            policy_already_exists = False
                            for policy in p_set:
                                if policy.decisions == policy_prime:
                                    policy_already_exists = True
                                    break

                            if not policy_already_exists:
                                new_exp_value = action.expected_reward + \
                                    (gamma * action.expected_case *
                                     nmatrix(pv.rewards[:, 0]).T)[0, 0]
                                exp_dominates = \
                                    all(new_exp_value > pv.rewards[state, 0])

                                pes_reward, pes_p = \
                                    action.fast_lower_bound(lower_order)
                                new_pes_value = \
                                    pes_reward + (gamma * pes_p *
                                                  nmatrix(pv.rewards[:, 1]).T
                                                  )[0, 0]
                                pes_dominates = \
                                    all(new_pes_value >= pv.rewards[state, 1])

                                opt_dominates = False
                                if dimensions == 3:
                                    opt_reward, opt_p = \
                                        action.fast_upper_bound(upper_order)
                                    new_opt_value = \
                                        opt_reward + (gamma * opt_p *
                                                      nmatrix(pv.rewards[:, 2]
                                                              ).T)[0, 0]
                                    opt_dominates = \
                                        all(new_opt_value >
                                            pv.rewards[state, 2])

                                if exp_dominates or pes_dominates or \
                                        opt_dominates:
                                    new_pv = \
                                        PolicyRewardTuple(num_states,
                                                          dimensions)
                                    new_pv.decisions = policy_prime
                                    self.evaluate_policy(new_pv, gamma)
                                    p_set.append(new_pv)
                                    insert_non_dominated(pv_prime_prime_set,
                                                         new_pv)

            equal = True
            for pv in pv_set:
                if not pv_prime_prime_set.__contains__(pv):
                    equal = False
                    break
            if equal is True:
                for pv in pv_prime_prime_set:
                    if not pv_set.__contains__(pv):
                        equal = False
                        break

            if equal:
                break
            else:
                pv_set = list(pv_prime_prime_set)
        return pv_set


# make a random MDP
def create_random_process(num_states, num_actions):
    """
    Creates a randomly generated MDP with a given number of states and actions.
    """
    generator = random.Random()
    actions = [[None for _ in range(num_actions)] for _ in range(num_states)]
    for s in range(num_states):
        for action_idx in range(num_actions):
            reward = generator.uniform(0, 10)
            # build transition vector
            transitions = np.random.rand(num_states,)

            t = transitions / sum(transitions)
            et = np.matrix(transitions) / sum(transitions)
            actions[s][action_idx] = \
                IntervalAction((reward, reward), (t, t), et, reward)
    return IntervalMarkovDecisionProcess(actions)


# read stuff
def read_imdp_from_file(filename):
    """
    Reads a (interval-valued) Markov decision process from a file

    @param filename the name of the file to read from
    """
    # the format is the so-called sparse matrix format.
    # We shall need 6|A| sparse matrices in the following order:
    # lower bound transition matrix
    # expected transition matrix
    # upper bound transition matrix
    # lower bound reward
    # upper bound reward
    # expected reward
    BLOCK_SIZE = 6
    LOWER_BOUND_MATRIX_OFFSET = 0
    EXPECTED_MATRIX_OFFSET = 1
    UPPER_BOUND_MATRIX_OFFSET = 2
    LOWER_BOUND_REWARD_OFFSET = 3
    EXPECTED_REWARD_OFFSET = 4
    UPPER_BOUND_REWARD_OFFSET = 5
    matrices = read_matrices_from_file(filename)
    num_actions = int(len(matrices) / BLOCK_SIZE)
    num_states = matrices[0].shape[0]
    # create states for actions
    actions = [[] for _ in range(num_states)]
    for st in range(num_states):
        for act in range(num_actions):
            lower_bound_matrix = matrices[act * BLOCK_SIZE +
                                          LOWER_BOUND_MATRIX_OFFSET]
            expected_matrix = matrices[act * BLOCK_SIZE +
                                       EXPECTED_MATRIX_OFFSET]
            upper_bound_matrix = matrices[act * BLOCK_SIZE +
                                          UPPER_BOUND_MATRIX_OFFSET]
            l_transition_vector = lower_bound_matrix[st, :]
            e_transition_vector = expected_matrix[st, :]
            u_transition_vector = upper_bound_matrix[st, :]

            lower_bound_reward = matrices[act * BLOCK_SIZE +
                                          LOWER_BOUND_REWARD_OFFSET]
            expected_reward = matrices[act * BLOCK_SIZE +
                                       EXPECTED_REWARD_OFFSET]
            upper_bound_reward = matrices[act * BLOCK_SIZE +
                                          UPPER_BOUND_REWARD_OFFSET]
            l_reward = lower_bound_reward[st, st]
            e_reward = expected_reward[st, st]
            u_reward = upper_bound_reward[st, st]

            action = IntervalAction((l_reward, u_reward),
                                    (l_transition_vector, u_transition_vector),
                                    e_transition_vector, e_reward)
            actions[st].push(action)

    return IntervalMarkovDecisionProcess(actions)


def read_matrices_from_file(filename):
    """
    Reads matrices from file defined in the "sparse matrix format"

    @param filename the name of the file to be read
    """
    with open(filename) as fd:
        content = fd.readlines()
        # parse line-by-line
        # first, strip all comment lines
        significant_lines = []
        for line in content:
            if line[0] != '#':
                significant_lines.append(line.strip())
        # then, read the number of matrices
        number_of_matrices = int(significant_lines[0])
        matrices = []
        number_of_states = 0
        # partition groups are somewhat interesting
        index = 1
        for m in range(number_of_matrices):
            partition_groups = int(significant_lines[index])
            index += 1
            number_of_states = int(significant_lines[index + partition_groups])
            # create matrix
            mtx = sp.identity(number_of_states, format='lil')
            index += partition_groups + 1
            for state in range(number_of_states):
                parts = significant_lines[index].split()
                index += 1
                deviants = int(parts[0])
                mtx[state, state] = float(parts[1])
                for d in range(deviants):
                    parts = significant_lines[index].split()
                    i = int(parts[0])
                    mtx[state, i] = float(parts[1])
                    index += 1
            matrices.append(mtx.todense())
        return matrices


def sparse_matrix_representation(out_matrix):
    """
    Returns a (string) sparse matrix representation of a matrix

    @param out_matrix The matrix to represent

    """
    return_string = "# new matrix\n"
    partition_groups = 1
    return_string += "{0} \n".format(partition_groups)
    dimension = out_matrix.shape[0]
    return_string += "0 \n{0} \n".format(dimension)
    for i in range(dimension):
        nonzero_indices = list(filter(lambda j:
                                      out_matrix[i, j] != 0 and i != j,
                                      range(dimension)))
        return_string += "{0} {1} \n".format(len(nonzero_indices),
                                             out_matrix[i, i])
        for j in nonzero_indices:
            return_string += "{0} {1} \n".format(j, out_matrix[i, j])

    return return_string


def dump_imdp(mdp):
    """
    Returns a (string) sparse matrix representation of a IMDP

    @param mdp The IMDP to be represented

    """
    assert isinstance(mdp, IntervalMarkovDecisionProcess)
    # first, find the number of matrices we shall need
    num_matrices = max([len(state_actions) for state_actions in mdp.actions])
    num_states = len(mdp.actions)

    ret_string = "{0}\n".format(num_matrices * 6)

    # for each action: create six matrices
    for a in range(num_matrices):
        lower_transition_matrix = nmatrix(np.zeros((num_states, num_states)))
        average_transition_matrix = nmatrix(np.zeros((num_states, num_states)))
        upper_transition_matrix = nmatrix(np.zeros((num_states, num_states)))
        lower_reward_matrix = nmatrix(np.zeros((num_states, num_states)))
        average_reward_matrix = nmatrix(np.zeros((num_states, num_states)))
        upper_reward_matrix = nmatrix(np.zeros((num_states, num_states)))

        for s in range(num_states):
            index = min(a, len(mdp.actions[s]) - 1)

            action = mdp.actions[s][index]
            assert isinstance(action, IntervalAction)

            lower_transition_matrix[s, :] = action.transitions[0]
            average_transition_matrix[s, :] = action.expected_case
            upper_transition_matrix[s, :] = action.transitions[1]

            lower_reward_matrix[s, s] = action.reward[0]
            average_reward_matrix[s, s] = action.expected_reward
            upper_reward_matrix[s, s] = action.reward[1]

        ret_string += "# action {0}\n".format(a)
        for m in [lower_transition_matrix, average_transition_matrix,
                  upper_transition_matrix, lower_reward_matrix,
                  average_reward_matrix, upper_reward_matrix]:
            ret_string += sparse_matrix_representation(m)

    return ret_string


def create_random_imdp(num_states, num_actions, difference, avg_reward,
                       var_reward):
    MATRICES = 6
    decimals = 4
    exponent = 1e4
    string = "# A random IMDP \n{0} \n".format(MATRICES * num_actions)
    for act in range(num_actions):
        # generate matrices
        # zeros = np.zeros((num_states, num_states))
        random_matrix = np.random.rand(num_states, num_states)
        random_matrix = random_matrix / sum(random_matrix)
        random_matrix *= exponent
        random_matrix = np.floor(random_matrix)
        delta = exponent - sum(random_matrix)
        for st in range(num_states):
            if delta[st] > 0:
                random_matrix[0, st] += delta[st]
            elif delta[st] < 0:
                # find some nonzero index
                for s in range(num_states):
                    if random_matrix[s, st] > -delta[st]:
                        random_matrix[s, st] += delta[st]
                        break
        random_matrix = np.clip(random_matrix.T, 0.0, exponent)
        random_matrix /= exponent

        # assert np.all(sum(random_matrix.T) == 1.0)
        assert np.all(random_matrix >= 0.0)

        difference_matrix = np.random.rand(num_states, num_states)
        difference_matrix = difference_matrix / sum(difference_matrix)
        lower_bound_matrix = \
            np.clip(random_matrix - (difference_matrix * difference),
                    0.0, 1.0).round(decimals)

        difference_matrix = np.random.rand(num_states, num_states)
        difference_matrix = difference_matrix / sum(difference_matrix)
        upper_bound_matrix = \
            np.clip(random_matrix + (difference_matrix * difference),
                    0.0, 1.0).round(decimals)

        assert np.all(lower_bound_matrix <= random_matrix) and \
            np.all(random_matrix <= upper_bound_matrix)

        reward_matrix = \
            np.diag(np.diag(np.random.rand(num_states,
                                           num_states))) * avg_reward
        lower_reward_bound_matrix = \
            np.diag(np.diag(reward_matrix -
                            np.random.rand(num_states,
                                           num_states) * var_reward))
        upper_reward_bound_matrix = \
            np.diag(np.diag(reward_matrix +
                            np.random.rand(num_states,
                                           num_states) * var_reward))

        # write them out
        # first, the transition matrices:
        string += "# action {0} \n".format(act)
        for m in [lower_bound_matrix, random_matrix, upper_bound_matrix,
                  lower_reward_bound_matrix, reward_matrix,
                  upper_reward_bound_matrix]:
            string += sparse_matrix_representation(m)
    return string


def dump_imdp_from_matrices(lower_bound_matrices, average_matrices, upper_bound_matrices,
                            lower_reward_bounds, average_rewards, upper_reward_bounds):
    MATRICES = 6
    decimals = 5
    exponent = 1e4
    num_actions = len(lower_bound_matrices)
    string = "# A IMDP \n{0} \n".format(MATRICES * num_actions)
    for act in range(num_actions):
        string += "# action {0} \n".format(act)
        assert np.all(lower_bound_matrices[act] <= average_matrices[act])
        assert np.all(average_matrices[act] <= upper_bound_matrices[act])
        for m in [lower_bound_matrices[act], average_matrices[act], upper_bound_matrices[act],
                  np.diag(np.array(lower_reward_bounds[act]).flatten()),
                  np.diag(np.array(average_rewards[act]).flatten()),
                  np.diag(np.array(upper_reward_bounds[act]).flatten())]:
            string += sparse_matrix_representation(m)
    return string


def flatten_coordinates_function(limits):
    """
    Computes a coordinates flattening function.

    @param limits: the box dimensions
    @return: A function that takes box coordinates and returns a flat coordinate
    """
    def flatten(values):
        coordinate = 0.0
        for i, value in enumerate(values):
            multiplier = int(np.product(limits[i+1:]))
            coordinate += value * multiplier
        return coordinate
    return flatten


def unflatten_coordinates_function(limits):
    """
    Computes a coordinates unrolling function.

    @param limits: the box dimensions
    @return: A function that takes a flat coordinate and returns box coordinates
    """
    def unflatten(value):
        coords = [0] * len(limits)
        for i, _ in enumerate(limits):
            multiplier = int(np.product(limits[i+1:]))
            coordinate = int(value / multiplier)
            coords[i] = coordinate
            value -= coordinate * multiplier
        return coords
    return unflatten


def create_random_grid_model(grid_width, grid_depth):
    state_list = list(itertools.product(range(grid_width), range(grid_depth)))
    action_list = range(grid_depth)
    num_states = len(state_list)
    # NOTE 1 and 10 are magic numbers. You are free to change them.
    dirichlet_parameters = np.ones((1, grid_depth))
    actions = [list() for _ in range(num_states)]

    state_index = np.zeros((grid_width, grid_depth))
    for idx, s in enumerate(state_list):
        (i, j) = s
        state_index[i, j] = idx

    for j in action_list:
        params = np.array(dirichlet_parameters)
        params[0, j] = 10  # NOTE magic number! You may change it.
        for s in range(num_states):
            random_vector = np.random.dirichlet(params[0, :], 1)
            absolute_minimum = np.zeros((1, grid_depth))
            absolute_maximum = np.ones((1, grid_depth))
            randomness_mean = 0.2  # NOTE again, magic number
            randomness_variance = 0.05  # NOTE magic number
            low_noise = np.random.normal(randomness_mean, randomness_variance,
                                         grid_depth)
            high_noise = np.random.normal(randomness_mean, randomness_variance,
                                          grid_depth)
            rand_low = np.maximum(absolute_minimum,
                                  random_vector - np.absolute(low_noise))
            rand_high = np.minimum(absolute_maximum,
                                   random_vector + np.absolute(high_noise))

            reward_mean = 100
            reward_variance = 20
            random_reward = max(0, np.random.normal(reward_mean,
                                                    reward_variance))

            transition_vector_lower = np.matrix(np.zeros((1, num_states)))
            transition_vector_upper = np.matrix(np.zeros((1, num_states)))
            transition_vector_expected = np.matrix(np.zeros((1, num_states)))

            state_unrolled = state_list[s]
            (i, _) = state_unrolled

            for j_next in range(grid_depth):
                next_state_index = state_index[max(i, grid_width - 1), j_next]
                transition_vector_lower[0, next_state_index] = \
                    rand_low[0, j_next]
                transition_vector_upper[0, next_state_index] = \
                    rand_high[0, j_next]
                transition_vector_expected[0, next_state_index] = \
                    random_vector[0, j_next]

            reward_lower = \
                max(0,
                    random_reward - np.absolute(
                        np.random.normal(reward_variance, reward_variance / 2)))
            reward_upper = \
                random_reward + np.absolute(
                    np.random.normal(reward_variance, reward_variance / 2))
            action = IntervalAction((reward_lower, reward_upper),
                                    (transition_vector_lower,
                                     transition_vector_upper),
                                    transition_vector_expected, random_reward)
            actions[s].append(action)
    return IntervalMarkovDecisionProcess(actions)


def create_tour_guide_model(width, uncertainty):
    """
    Creates a tour guide model as described in [1].
    The point of the exercise is to create a realistic case study.

    [1] Reward-Bounded Reachability Probability for Uncertain Weighted MDPs
    """
    limits = [width, width]
    flatten = flatten_coordinates_function(limits)
    unflatten = unflatten_coordinates_function(limits)

    # define constants
    # rewards
    low_importance_reward = 1.0
    medium_importance_reward = 2.0
    high_importance_reward_low = 3.0
    high_importance_reward_high = 4.0
    # transitions
    transition_probability_main_direction_low = 1.0 - uncertainty
    transition_probability_main_direction_high = 1.0
    transition_probability_secondary_direction_low = 0.0
    transition_probability_secondary_direction_high = 0.0 + uncertainty

    transition_probability_default_main_direction_low = 1.0
    transition_probability_default_main_direction_high = 1.0
    transition_probability_default_secondary_direction_low = 0.0
    transition_probability_default_secondary_direction_high = 0.0

    nstates = width * width
    actions = [None] * nstates
    m = float(width - 1) / 2.0

    outer_boundary = float(width) / 5
    inner_boundary = float(width) / 10

    for state in range(nstates):
        [i, j] = unflatten(state)
        state_reward_low = 0.0
        state_reward_high = 0.0
        state_reward_exp = 0.0
        # compute how many neighbors we have
        num_neighbors = 9
        if (i == 0 or i == width - 1) and (j == 0 or j == width - 1):
            num_neighbors = 4
        elif i == 0 or j == 0 or i == width - 1 or j == width - 1:
            num_neighbors = 6

        # compute state reward
        distance = max(abs(i - m), abs(j - m))
        if distance > outer_boundary:
            state_reward_low = low_importance_reward
            state_reward_exp = low_importance_reward
            state_reward_high = low_importance_reward
            tp_main_low = transition_probability_default_main_direction_low
            tp_main_high = transition_probability_default_main_direction_high
            tp_main_avg = (tp_main_low + tp_main_high) * 0.5
            tp_secondary_low = \
                transition_probability_default_secondary_direction_low
            tp_secondary_high = \
                transition_probability_default_secondary_direction_high
            tp_secondary_avg = (tp_secondary_low + tp_secondary_high) * 0.5
        elif distance > inner_boundary:
            state_reward_low = medium_importance_reward
            state_reward_exp = medium_importance_reward
            state_reward_high = medium_importance_reward
            tp_main_low = transition_probability_default_main_direction_low
            tp_main_high = transition_probability_default_main_direction_high
            tp_main_avg = (tp_main_low + tp_main_high) * 0.5
            tp_secondary_low = \
                transition_probability_default_secondary_direction_low
            tp_secondary_high = \
                transition_probability_default_secondary_direction_high
            tp_secondary_avg = (tp_secondary_low + tp_secondary_high) * 0.5
        else:
            state_reward_low = high_importance_reward_low
            state_reward_high = high_importance_reward_high
            state_reward_exp = (state_reward_low + state_reward_high) / 2
            tp_main_low = transition_probability_main_direction_low
            tp_main_high = transition_probability_main_direction_high
            tp_main_avg = 1.0 - uncertainty
            tp_secondary_low = transition_probability_secondary_direction_low
            tp_secondary_high = transition_probability_secondary_direction_high
            tp_secondary_avg = uncertainty / float(num_neighbors - 1)
        # create two actions
        east_transition_low = np.matrix(np.zeros((1, nstates)))
        east_transition_avg = np.matrix(np.zeros((1, nstates)))
        east_transition_high = np.matrix(np.zeros((1, nstates)))

        west_transition_low = np.matrix(np.zeros((1, nstates)))
        west_transition_avg = np.matrix(np.zeros((1, nstates)))
        west_transition_high = np.matrix(np.zeros((1, nstates)))

        for i_prime in [i - 1, i, i + 1]:
            for j_prime in [j - 1, j, j + 1]:
                i1 = np.clip(i_prime, 0, width - 1)
                j1 = np.clip(j_prime, 0, width - 1)
                state_coord = flatten([i1, j1])
                east_transition_low[0, state_coord] = tp_secondary_low
                west_transition_low[0, state_coord] = tp_secondary_low
                east_transition_avg[0, state_coord] = tp_secondary_avg
                west_transition_avg[0, state_coord] = tp_secondary_avg
                east_transition_high[0, state_coord] = tp_secondary_high
                west_transition_high[0, state_coord] = tp_secondary_high
        i_west, j_west, i_east, j_east = 0, 0, 0, 0
        if i <= j:
            i_west = min(i + 1, width - 1)
            i_east = min(i + 1, width - 1)
            j_west = min(j + 1, width - 1)
            j_east = max(j - 1, 0)
        if i >= j:
            j_west = min(j + 1, width - 1)
            j_east = min(j + 1, width - 1)
            i_west = min(i + 1, width - 1)
            i_east = max(i - 1, 0)
        west_coord = flatten([i_west, j_west])
        east_coord = flatten([i_east, j_east])

        east_transition_low[0, east_coord] = \
            west_transition_low[0, west_coord] = tp_main_low

        east_transition_avg[0, east_coord] = \
            west_transition_avg[0, west_coord] = tp_main_avg
        assert(abs(np.sum(east_transition_avg) - 1.0) < 1e-5)
        assert(abs(np.sum(west_transition_avg) - 1.0) < 1e-5)

        east_transition_high[0, east_coord] = \
            west_transition_high[0, west_coord] = tp_main_high

        west_action = IntervalAction((state_reward_low, state_reward_high),
                                     (west_transition_low,
                                      west_transition_high),
                                     west_transition_avg, state_reward_exp)
        east_action = IntervalAction((state_reward_low, state_reward_high),
                                     (east_transition_low,
                                      east_transition_high),
                                     east_transition_avg, state_reward_exp)
        actions[state] = [east_action, west_action]
    return IntervalMarkovDecisionProcess(actions)


def create_random_queueing_model(num_servers, queue_length):
    mean_arrival_probability = 0.3  # NOTE magic number
    mean_service_probability = 0.1  # NOTE magic number
    mean_start_probability = 0.5  # NOTE magic number

    energy_usage_off = 0.5
    energy_usage_starting = 7.5
    energy_usage_on = 5

    randomness_mean = 0.01
    randomness_variance = 0.005

    states = []
    state_reverse_index = {}
    state_index = 0

    for jobs_in_queue in range(queue_length + 1):
        for working_servers in range(num_servers + 1):
            for stopped_servers in range(num_servers - working_servers + 1):
                starting_servers = \
                    num_servers - working_servers - stopped_servers
                state = (jobs_in_queue, working_servers,
                         stopped_servers, starting_servers)
                states.append(state)
                state_reverse_index[state] = state_index
                state_index += 1

    num_states = len(states)
    actions = [[] for _ in states]

    for i, (jobs, running, stopped, starting) in enumerate(states):
        # action 0: ignore
        # then, we compute the transition probabilities
        for action in range(num_servers + 1):
            shutdown = action
            if shutdown > running:
                excess = shutdown - running
                wakeup = min(excess, stopped)
                shutdown = -wakeup

            transition_vector = np.matrix(np.zeros((1, num_states)))
            transition_vector_lower = np.matrix(np.zeros((1, num_states)))
            transition_vector_upper = np.matrix(np.zeros((1, num_states)))

            total_probability = 0.0
            for start in range(starting + 1):
                start_probability = \
                    scipy.special.binom(starting, start) * \
                    (mean_start_probability ** start) * \
                    ((1.0 - mean_start_probability) ** (starting - start))
                current_service_probability = \
                    1.0 - (1.0 - mean_service_probability) ** running
                service_arrival_probability = \
                    start_probability * current_service_probability * \
                    mean_arrival_probability
                service_no_arrival_probability = \
                    start_probability * current_service_probability * \
                    (1.0 - mean_arrival_probability)
                no_service_arrival_probability = \
                    start_probability * (1.0 - current_service_probability) * \
                    mean_arrival_probability
                no_service_no_arrival_probability = \
                    start_probability * (1.0 - current_service_probability) * \
                    (1.0 - mean_arrival_probability)

                # generate some noise
                low_noise = np.absolute(
                    np.random.normal(randomness_mean, randomness_variance, 4))
                high_noise = np.absolute(
                    np.random.normal(randomness_mean, randomness_variance, 4))

                service_no_arrival_state = \
                    state_reverse_index[(max(0, jobs - 1),
                                         running + start - shutdown,
                                         stopped + shutdown, starting - start)]
                transition_vector[0, service_no_arrival_state] += \
                    service_no_arrival_probability
                transition_vector_lower[0, service_no_arrival_state] += \
                    max(0, service_no_arrival_probability - low_noise[0])
                transition_vector_upper[0, service_no_arrival_state] += \
                    min(1, service_no_arrival_probability + high_noise[0])

                service_arrival_state = \
                    state_reverse_index[(jobs, running + start - shutdown,
                                         stopped + shutdown, starting - start)]
                transition_vector[0, service_arrival_state] += \
                    service_arrival_probability
                transition_vector_lower[0, service_arrival_state] += \
                    max(0, service_arrival_probability - low_noise[1])
                transition_vector_upper[0, service_arrival_state] += \
                    min(1, service_arrival_probability + high_noise[1])

                no_service_arrival_state = \
                    state_reverse_index[(min(queue_length, jobs + 1),
                                         running + start - shutdown,
                                         stopped + shutdown, starting - start)]
                transition_vector[0, no_service_arrival_state] += \
                    no_service_arrival_probability
                transition_vector_lower[0, no_service_arrival_state] += \
                    max(0, no_service_arrival_probability - low_noise[2])
                transition_vector_upper[0, no_service_arrival_state] += \
                    min(1, no_service_arrival_probability + high_noise[2])

                no_service_no_arrival_state = \
                    state_reverse_index[(jobs, running + start - shutdown,
                                         stopped + shutdown, starting - start)]
                transition_vector[0, no_service_no_arrival_state] += \
                    no_service_no_arrival_probability
                transition_vector_lower[0, no_service_no_arrival_state] += \
                    max(0, no_service_no_arrival_probability - low_noise[2])
                transition_vector_upper[0, no_service_no_arrival_state] += \
                    min(1, no_service_no_arrival_probability + high_noise[2])

            total_probability = np.sum(transition_vector)
            assert total_probability >= (1.0 - 1e-7), "Probabilities messed up!"

            reward = (num_servers - jobs) / \
                     (running * energy_usage_on + stopped * energy_usage_off +
                      starting * energy_usage_starting)

            mdp_action = IntervalAction((reward, reward),
                                        (transition_vector_lower,
                                         transition_vector_upper),
                                        transition_vector, reward)
            actions[i].append(mdp_action)
    return IntervalMarkovDecisionProcess(actions)
