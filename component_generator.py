#!/bin/env python3
from optparse import OptionParser

import numpy as np
from typing import List, Iterable, Tuple
from enum import Enum
import cmdp
import imdp


np.core.arrayprint._line_width = 150


class ApproximationMode(Enum):
    upper_bound = 1
    lower_bound = 2
    expected_case = 3


class Scenario(Enum):
    all_lower_bounds = 0
    all_upper_bounds = 1
    fast_degrading_slow_repair = 2
    slow_degrading_fast_repair = 3
    expected_case = 4


def component_operational_functions(stage: int) -> Tuple[np.matrix, np.matrix, float]:
    phi = np.matrix([[1, 0, 0]])
    base_rate = stage + 1
    D = np.matrix([[-base_rate, base_rate, 0],
                   [0, -base_rate, base_rate],
                   [0, 0, -base_rate]])
    reward = 10.0 - 1.0 / (stage + 1)
    return phi, D, reward


def component_repair_functions(stage: int) -> Tuple[np.matrix, np.matrix, float]:
    phi, D, reward = component_operational_functions(stage)
    return phi, D * 10.0, reward / 2

phi_replace = np.matrix([[1, 0, 0, 0]])
D_replace = np.matrix([[-3.0, 2.9, 0.0, 0.0],
                       [0.0, -1.0, 0.9, 0.0],
                       [0.0, 0.0, -0.5, 0.4],
                       [0.0, 0.0, 0.0, -1.0]])
reward_replace = 0.0

component_replace_function = (phi_replace, D_replace, reward_replace)


def gen_failure_distribution(stages: int):
    return [1/(x + 1) for x in range(stages)]


def gen_repair_distribution(stages: int):
    transfer_matrix = np.eye(stages)
    return transfer_matrix


def gen_degradation_distribution(stages: int):
    transfer_matrix = np.zeros((stages, stages))
    for j in range(stages - 1):
        transfer_matrix[j + 1, j] = 1.0
    return transfer_matrix


def approximate_phd(phi: np.matrix, D: np.matrix, mode: ApproximationMode):
    if mode is not ApproximationMode.expected_case:
        row_sums = np.sum(-D, 1)
        if mode is ApproximationMode.upper_bound:
            return np.max(row_sums)
        else:
            rate = np.max(row_sums)
            mass = 1.0
            t = 0.0
            while mass > 1.0e-5:
                t += 0.1
                mass_vector = phi * np.expm1(D * t)
                mass = np.sum(mass_vector)
                mass_vector_normalized = mass_vector
                if np.isnan(mass):
                    mass = 0.0
                if mass > 0.0:
                    mass_vector_normalized = mass_vector / (np.sum(mass_vector))
                current_rate = np.maximum(1e-5, mass_vector_normalized * row_sums)
                if not np.isnan(current_rate):
                    rate = np.minimum(rate, current_rate)
            if not np.isnan(rate):
                return np.maximum(rate, 1e-5)
            else:
                return 1e-5
    else:
        return -1.0 / np.sum(phi * D.I)


def generate_single_component(num_stages: int, component_operational_functions, component_repair_functions,
                              component_replace_function, failure_distribution: np.array,
                              degradation_distribution: np.matrix, repair_distribution: np.matrix,
                              scenario: Scenario) -> Tuple[np.matrix, np.matrix, np.matrix]:
    component_dimensions = 2 * num_stages
    Qignore = np.zeros((component_dimensions, component_dimensions))
    Qrepair = np.zeros((component_dimensions, component_dimensions))

    if scenario in [Scenario.all_lower_bounds, Scenario.fast_degrading_slow_repair]:
        repair_approximation = ApproximationMode.lower_bound
    elif scenario in [Scenario.all_upper_bounds, Scenario.slow_degrading_fast_repair]:
        repair_approximation = ApproximationMode.upper_bound
    else:
        repair_approximation = ApproximationMode.expected_case

    if scenario in [Scenario.all_upper_bounds, Scenario.fast_degrading_slow_repair]:
        operate_approximation = ApproximationMode.upper_bound
    elif scenario in [Scenario.all_lower_bounds, Scenario.slow_degrading_fast_repair]:
        operate_approximation = ApproximationMode.lower_bound
    else:
        operate_approximation = ApproximationMode.expected_case

    def operational_stage_row_index(stage_index):
        return 2 * (num_stages - stage_index) - 2

    failure_stage_row_index = component_dimensions - 1

    def maintenance_stage_row_index(stage_index):
        return 2 * (num_stages - stage_index) - 1

    reward = np.zeros((component_dimensions, 1))

    for j in range(num_stages):
        row_index = operational_stage_row_index(j)
        failure_probability = failure_distribution[j]
        (phi, D, phase_reward) = component_operational_functions(j)
        rate = approximate_phd(phi, D, operate_approximation)
        for jprime in range(num_stages):
            row_index_prime = operational_stage_row_index(jprime)
            Qignore[row_index, row_index_prime] = (1 - failure_probability) * degradation_distribution[j, jprime] * rate
        Qignore[row_index, row_index] = -rate
        Qignore[row_index, failure_stage_row_index] = rate * failure_probability

        Qrepair[row_index, row_index] = -rate
        Qrepair[row_index, row_index + 1] = rate
        reward[row_index, 0] = phase_reward

    for j in range(num_stages - 1):
        row_index = maintenance_stage_row_index(j + 1)
        (phi, D, phase_reward) = component_repair_functions(j + 1)
        rate = approximate_phd(phi, D, repair_approximation)
        Qignore[row_index, row_index] = -rate
        for jprime in range(num_stages):
            row_index_prime = operational_stage_row_index(jprime)
            Qignore[row_index, row_index_prime] = repair_distribution[j + 1, jprime] * rate
        Qrepair[row_index, :] = Qignore[row_index, :]
        reward[row_index, 0] = phase_reward

    (phi, D, phase_reward) = component_replace_function
    repair_rate = approximate_phd(phi, D, repair_approximation)
    Qrepair[failure_stage_row_index, failure_stage_row_index] = -repair_rate
    Qrepair[failure_stage_row_index, 0] = repair_rate
    Qignore[failure_stage_row_index, :] = Qrepair[failure_stage_row_index, :]
    reward[failure_stage_row_index, 0] = phase_reward

    return np.matrix(Qignore), np.matrix(Qrepair), np.matrix(reward)


def kronecker_sum(a: np.matrix, b: np.matrix):
    assert a.shape[0] == a.shape[1] and b.shape[0] == b.shape[1]
    id_a = np.eye(a.shape[0])
    id_b = np.eye(b.shape[0])
    return np.kron(a, id_b) + np.kron(id_a, b)


def generate_component_model(num_components: int, repair_resources: int, num_stages: int,
                             component_operational_functions, component_repair_functions, component_replace_function,
                             failure_distribution: np.array,
                             degradation_distribution: np.matrix, repair_distribution: np.matrix,
                             scenario: Scenario):
    (Qignore, Qrepair, reward) = generate_single_component(num_stages, component_operational_functions,
                                                           component_repair_functions, component_replace_function,
                                                           failure_distribution, degradation_distribution,
                                                           repair_distribution, scenario)
    component_dimensions = 2 * num_stages

    def num_maintenance_phases(compound_state: List[int]):
        # the rationale is the following: maintenance phases are the ones with odd indices (1, 3, …) except for the last
        # phase which is the replacement phase
        return sum(map(lambda s: (s < component_dimensions - 1) and (s % 2 == 1), compound_state))

    available_tuples = [[]] * num_components

    available_tuples[0] = [[s] for s in range(component_dimensions)]
    all_tuples = list(available_tuples)
    for i in np.arange(1, num_components):
        Tprime = []
        new_tuples = []
        for state in available_tuples[i - 1]:
            for s in range(component_dimensions):
                new_state = state + [s]
                new_tuples.append(new_state)
                if num_maintenance_phases(new_state) <= repair_resources:
                    Tprime.append(new_state)
        all_tuples[i] = new_tuples
        available_tuples[i] = Tprime
        # print(all_tuples[i])
        # print(available_tuples[i])

    def to_state_number(compound_state):
        return all_tuples[len(compound_state) - 1].index(compound_state)

    available_states = [[]] * num_components
    for i in range(num_components):
        available_states[i] = list(map(to_state_number, available_tuples[i]))
        for j in available_states[i]:
            assert num_maintenance_phases(all_tuples[i][j]) <= repair_resources

    # print(available_states)

    repair_matrices = [np.matrix(0)] * num_components
    Qignoreall = Qignore

    reward_matrix = np.diag(np.array(reward).flatten())

    for i in np.arange(1, num_components):
        repair_matrices[i] = Qignoreall
        Qignoreall = kronecker_sum(Qignoreall, Qignore)
        Qignoreall = Qignoreall[available_states[i], :]
        Qignoreall = Qignoreall[:, available_states[i]]

        reward_matrix = kronecker_sum(reward_matrix, reward_matrix)
        reward_matrix = reward_matrix[available_states[i], :]
        reward_matrix = reward_matrix[:, available_states[i]]

    def merge_matrices(source_matrix: np.matrix, merge_matrix: np.matrix, indices: Iterable):
        m = source_matrix
        for k in range(m.shape[0]):
            if k in indices:
                m[k, :] = merge_matrix[k, :]
        return m

    for i in range(num_components):
        repair_matrices[i] = kronecker_sum(repair_matrices[i], np.matrix(Qrepair))
        repair_matrices[i] = repair_matrices[i][available_states[i], :]
        repair_matrices[i] = repair_matrices[i][:, available_states[i]]
        for j in np.arange(i + 1, num_components):
            # j runs in the range i + 1, i + 2, ... num_components - 1
            repair_matrix = kronecker_sum(repair_matrices[i], Qignore)

            repair_matrix = repair_matrix[available_states[j], :]
            repair_matrix = repair_matrix[:, available_states[j]]
            repair_matrices[i] = repair_matrix

        # now, remove the invalid transitions
        for k in range(repair_matrices[i].shape[0]):
            # we know that we have reduced the state space. Then, the invalid transition rate vectors will have row sum
            # not equal to zero, which is how we find them.
            row_sum = np.sum(repair_matrices[i][k, :])
            if row_sum < 0:
                repair_matrices[i][k, :] = Qignoreall[k, :]

    return [Qignoreall] + repair_matrices, np.matrix(np.diag(reward_matrix))


def uniformization(decisions: Iterable[np.matrix], reward: np.matrix, uniformization_rate: float,
                   discount_rate: float) -> Tuple[Iterable[np.matrix], np.matrix, float]:
    uniformized_matrices = [np.matrix(np.eye(rate_matrix.shape[0]) + rate_matrix / uniformization_rate)
                            for rate_matrix in decisions]
    uniformized_reward = reward / (uniformization_rate + discount_rate)
    discount_factor = uniformization_rate / (uniformization_rate + discount_rate)
    return uniformized_matrices, uniformized_reward, discount_factor


if __name__ == "__main__":
    parser = OptionParser(
        usage="usage: %prog -c,--components=[num_components] "
              "-p,--phases=[num_phases] "
              "-r,--repair-workers=[num_repair_workers] "
              "-a,--alpha=[discount_rate]")
    parser.add_option("-p", "--phases", action="store", type="int", dest="stages", default=2)
    parser.add_option("-c", "--components", action="store", type="int", dest="components", default=2)
    parser.add_option("-r", "--repair_workers", action="store", type="int", dest="repair_resources", default=1)
    parser.add_option("-a", "--alpha", action="store", type="float", dest="alpha", default=0.01)
    (options, args) = parser.parse_args()

    stages = options.stages
    num_components = options.components
    num_repair_resources = options.repair_resources
    alpha = options.alpha
    print("Generate a model with {0} components, {1} operational phases, {2} repair resources and discount rate {3}".
          format(num_components, stages, num_repair_resources, alpha))

    (Qignore, Qrepair, reward_single) = generate_single_component(stages, component_operational_functions,
                                                                  component_repair_functions,
                                                                  component_replace_function,
                                                                  gen_failure_distribution(stages),
                                                                  gen_degradation_distribution(stages),
                                                                  gen_repair_distribution(stages),
                                                                  Scenario.all_upper_bounds)

    num_scenarios = len(Scenario)
    scenarios = [[]] * num_scenarios
    discrete_scenarios = [[]] * num_scenarios
    beta_star = 0
    reward = None
    discrete_reward = None
    gamma = None
    for scenario in Scenario:
        scenarios[scenario.value], reward = generate_component_model(num_components, num_repair_resources, stages,
                                                                     component_operational_functions,
                                                                     component_repair_functions,
                                                                     component_replace_function,
                                                                     gen_failure_distribution(stages),
                                                                     gen_degradation_distribution(stages),
                                                                     gen_repair_distribution(stages), scenario)
        beta = -min([np.min(matrix) for matrix in scenarios[scenario.value]])
        beta_star = max(beta, beta_star)

    for scenario in Scenario:
        (discrete_scenarios[scenario.value], discrete_reward, gamma) = uniformization(scenarios[scenario.value], reward,
                                                                                      beta_star, alpha)


    imdp_lower_bound_matrices = []
    imdp_average_matrices = []
    imdp_upper_bound_matrices = []

    num_actions = num_components + 1

    for act in range(num_actions):
        imdp_lower_bound_matrices.append(np.min([discrete_scenarios[s.value][act] for s in Scenario], axis=0))
        imdp_upper_bound_matrices.append(np.max([discrete_scenarios[s.value][act] for s in Scenario], axis=0))
        imdp_average_matrices.append(discrete_scenarios[Scenario.expected_case.value][act])
    action_rewards = [discrete_reward] * num_actions

    imdp_string = imdp.dump_imdp_from_matrices(imdp_lower_bound_matrices, imdp_average_matrices,
                                               imdp_upper_bound_matrices,
                                               action_rewards, action_rewards, action_rewards)

    cmdp_iniv = np.zeros(reward.shape[1])
    cmdp_iniv[0] = 1.0

    interesting_scenarios = [Scenario.fast_degrading_slow_repair,
                             Scenario.expected_case,
                             Scenario.slow_degrading_fast_repair]

    cmdp = cmdp.ConcurrentMarkovDecisionProcess(matrices=[discrete_scenarios[s.value] for s in interesting_scenarios],
                                                rewards=([np.array(discrete_reward).flatten()] *
                                                len(interesting_scenarios)),
                                                iniv=cmdp_iniv.flatten(), weights=[0.3, 0.4, 0.3])

    cmdp_string = cmdp.dump_as_string()

    model_name_string = "component_model_c{0}_r{1}_s{2}".format(num_components, num_repair_resources, stages)

    model_strings = [cmdp_string, imdp_string]
    model_representations = ["cmdp", "mdpu"]
    print("{1} states\n{0} discount".format(gamma, discrete_scenarios[0][0].shape[0]))

    for i, representation in enumerate(model_representations):
        filename = "{0}.{1}".format(model_name_string, representation)
        with open(filename, "w+") as fd:
            print(model_strings[i], file=fd)
