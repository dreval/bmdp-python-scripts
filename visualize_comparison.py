#!/usr/bin/env python3
# -*-coding: utf-8-*-

import numpy as np
import matplotlib
matplotlib.use('PDF')
from matplotlib import pyplot as plt
from optparse import OptionParser


if __name__ == "__main__":
    parser = OptionParser(usage="usage: %prog [-f file]")
    parser.add_option("-f", "--file", action="store", type="string",
                      dest="filename")
    (options, args) = parser.parse_args()

    if options.filename is None:
        filename = "testcases_queue/ipt_evo_comparison"
    else:
        filename = options.filename

    data = np.loadtxt(filename, delimiter=";", skiprows=1)
    size = data[:, 0]
    c_ie = data[:, 4]
    c_ei = data[:, 5]
    fields = 'f8,f8'
    c_ie_data = np.array([size, c_ie])
    c_ei_data = np.array([size, c_ei])

    c_ie_data.view(fields).sort(axis=0, order='f0')

    c_ei_data.view(fields).sort(axis=0, order='f0')

    # now, plot something
    plt.plot(c_ie_data[1, :], c_ie_data[0, :], 'bo')
    plt.xlabel('states')
    plt.ylabel('C(IPT, SPEA2)')
    plt.ylim(0.5, 1.05)
    plt.savefig("c_ipt_evo.pdf")
    plt.show()
    plt.close()

    plt.plot(c_ei_data[1, :], c_ei_data[0, :], 'ro')
    plt.xlabel('states')
    plt.ylabel('C(SPEA2, IPT)')
    plt.savefig("c_evo_ipt.pdf")
    plt.show()
    plt.close()

    ipt_time_data_file = np.loadtxt("testcases_queue/queue_ipt_statistics",
                                    delimiter=";", skiprows=1)
    evo_time_data_file = np.loadtxt("testcases_queue/queue_evo_statistics",
                                    delimiter=";", skiprows=1)
    ipt_time_data = np.array([ipt_time_data_file[:, 0],
                              ipt_time_data_file[:, 3]])
    # ipt_time_data.view(fields).sort(axis=0, order='f1')
    evo_time_data = np.array([evo_time_data_file[:, 0],
                              evo_time_data_file[:, 3]])
    # evo_time_data.view(fields).sort(axis=0, order='f1')
    plt.plot(ipt_time_data[0, :], ipt_time_data[1, :], 'bo')
    plt.plot(evo_time_data[0, :], evo_time_data[1, :], 'ro')
    plt.savefig("evo_ipt_time.pdf")
    plt.show()
    plt.close()
