#!/usr/bin/env python3
# -*-coding: utf-8-*-

import numpy as np
import matplotlib
matplotlib.use('PDF')
from matplotlib import pyplot as plt
from optparse import OptionParser
from scipy import stats
import numpy.polynomial.polynomial as poly

if __name__ == "__main__":
    parser = OptionParser(usage="usage: %prog [-f file]")
    parser.add_option("-f", "--file", action="store", type="string", dest="filename")
    (options, args) = parser.parse_args()

    if options.filename is None:
        filename = "testcases/statistics"
    else:
        filename = options.filename

    data = np.loadtxt(filename, delimiter=";", skiprows=1)
    x = data[:, 0]
    y = data[:, 3]

    # do some fine regression curves
    fit_1 = poly.polyfit(x, y, 1)
    fit_2 = poly.polyfit(x, y, 3)
    fit_fn_1 = lambda x: poly.polyval(x, fit_1)
    fit_fn_2 = lambda x: poly.polyval(x, fit_2)

    # compute means and error bars
    values_map = {}
    for i, x_value in enumerate(x):
        if x_value in values_map.keys():
            values_map[x_value].append(y[i])
        else:
            values_map[x_value] = [y[i]]

    means = map(np.mean, values_map)
    errors = {}
    for x_value in values_map.keys():
        entries = values_map[x_value]
        errors[x_value] = stats.t.interval(0.95, df=(len(entries) - 1),
                                           loc=np.mean(entries), scale=np.std(entries)/np.sqrt(len(entries)))
    errorbar_xs = np.sort(np.array(list(values_map.keys())))
    errorbar_ys = np.array(list(map(lambda x_value: np.mean(values_map[x_value]), errorbar_xs)))
    errorbar_lower_error = np.array(list(map(lambda x_value: errors[x_value][0], errorbar_xs)))
    errorbar_upper_error = np.array(list(map(lambda x_value: errors[x_value][1], errorbar_xs)))

    plt.plot(x, y, 'r.')
    plt.errorbar(errorbar_xs, errorbar_ys, yerr=[errorbar_ys - errorbar_lower_error, errorbar_upper_error - errorbar_ys], fmt='+')
    plt.xlabel("states")
    plt.ylabel("seconds")
    plt.show()

    mean_fit_1_coeffs = poly.polyfit(errorbar_xs, errorbar_ys, 1)
    mean_fit_2_coeffs = poly.polyfit(errorbar_xs, errorbar_ys, 3)
    print("linear: mean fit {0}, ordinary fit {1}".format(mean_fit_1_coeffs, fit_1))
    print("quadratic: mean fit {0}, ordinary fit {1}".format(mean_fit_2_coeffs, fit_2))

    # first plot: total time
    plt.plot(x, y, 'r.', # x, fit_fn_1(x), 'b--', x, fit_fn_2(x), 'g--',
             #errorbar_xs, poly.polyval(errorbar_xs, mean_fit_1_coeffs), 'c-',
             errorbar_xs, poly.polyval(errorbar_xs, mean_fit_2_coeffs), 'g--')
    plt.errorbar(errorbar_xs, errorbar_ys,
                 yerr=[errorbar_ys - errorbar_lower_error, errorbar_upper_error - errorbar_ys], fmt='+')
    plt.xlabel("states")
    plt.ylabel("seconds")
    plt.savefig("total_time_plot.pdf")
    plt.show()

    plt.clf()
    # second plot: policy-adjusted total time
    policies = data[:, 4]
    adjusted_y = list(map(lambda yp: yp[0]/yp[1], zip(y, policies)))
    linear_fit = np.polyfit(x, adjusted_y, 1)
    linear_fit_fn = np.poly1d(linear_fit)
    quadratic_fit = np.polyfit(x, adjusted_y, 3)

    policy_ys_map = {}
    for i, x_value in enumerate(x):
        if x_value in policy_ys_map.keys():
            policy_ys_map[x_value].append(adjusted_y[i])
        else:
            policy_ys_map[x_value] = [adjusted_y[i]]
    adjusted_y_means = np.array(list(map(lambda x_value: np.mean(policy_ys_map[x_value]), errorbar_xs)))
    adjusted_errors = {}
    for x_value in errorbar_xs:
        entries = policy_ys_map[x_value]
        adjusted_errors[x_value] = stats.t.interval(0.95, df=(len(entries) - 1),
                                                    loc=np.mean(entries), scale=np.std(entries)/np.sqrt(len(entries)))
    adjusted_ys_lower_error = np.array(list(map(lambda x_value: adjusted_errors[x_value][0], errorbar_xs)))
    adjusted_ys_upper_error = np.array(list(map(lambda x_value: adjusted_errors[x_value][1], errorbar_xs)))

    adjusted_mean_fit_1 = np.polyfit(errorbar_xs, adjusted_y_means, 1)
    adjusted_mean_fit_2 = np.polyfit(errorbar_xs, adjusted_y_means, 3)

    plt.plot(x, adjusted_y, 'r.',
             #np.sort(x), np.poly1d(adjusted_mean_fit_1)(np.sort(x)), 'c-',
             np.sort(x), np.poly1d(adjusted_mean_fit_2)(np.sort(x)), 'g--')
    plt.errorbar(errorbar_xs, adjusted_y_means,
                 yerr=[adjusted_y_means - adjusted_ys_lower_error, adjusted_ys_upper_error - adjusted_y_means], fmt='+')
    plt.xlabel("states")
    plt.ylabel("seconds/policy")
    plt.savefig("time_per_policy_plot.pdf")

    plt.show()
