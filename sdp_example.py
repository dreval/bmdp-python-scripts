#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Code for the SDP example from the paper [1]

[1] …
"""
import sdp
import mdp
import numpy as np

__author__ = 'Dimitri Scheftelowitsch'

l = 1.0
m1 = 0.5
m2 = 4.0

D_01 = np.matrix([[-l,  l],
                  [0.0, -l]])
pi_01 = np.matrix([1.0, 0.0])

D_02 = np.matrix(D_01)
pi_02 = np.matrix(pi_01)

D_0r = np.matrix([[-m1, m1],
                  [0.0, -m2]])
pi_0r = np.matrix([1.0, 0.0])

c_1 = sdp.PHClock(D_01, pi_01)
c_2 = sdp.PHClock(D_02, pi_02)
c_r = sdp.PHClock(D_0r, pi_0r)

# act_uu = sdp.Action(np.matrix([1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]))
# act_ud = sdp.Action(np.matrix([0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0]))
# act_du = sdp.Action(np.matrix([0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0]))
# act_dd = sdp.Action(np.matrix([0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0]))
# act_ru = sdp.Action(np.matrix([0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0]))
# act_ur = sdp.Action(np.matrix([0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0]))
# act_rr = sdp.Action(np.matrix([0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0]))

act_uu_1_repair = sdp.Action(np.matrix([0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0]), [2])
act_uu_1_repair.reset = [2]

act_uu_2_repair = sdp.Action(np.matrix([0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0]), [2])
act_uu_2_repair.reset = [2]

act_uu_1_ignore = sdp.Action(np.matrix([0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0]), [])
act_uu_2_ignore = sdp.Action(np.matrix([0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0]), [])

act_du_2_repair = sdp.Action(np.matrix([0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0]), [])
act_ud_1_repair = sdp.Action(np.matrix([0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0]), [])

act_du_2_ignore = sdp.Action(np.matrix([0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0]), [])
act_ud_1_ignore = sdp.Action(np.matrix([0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0]), [])

act_r_repair = sdp.Action(np.matrix([0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0]), [])

act_rr_r = sdp.Action(np.matrix([1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]), [0, 1])
act_rr_r.reset = [0, 1]

act_ru_r = sdp.Action(np.matrix([1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]), [0])
act_ru_r.reset = [0]

act_ur_r = sdp.Action(np.matrix([1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]), [1])
act_ur_r.reset = [1]

uu = sdp.Location()
uu.actions = {0: [act_uu_1_ignore, act_uu_1_repair],
              1: [act_uu_2_ignore, act_uu_2_repair]}
uu.enabled = [0, 1]
uu.reward = 10

ud = sdp.Location()
ud.enabled = [0]
ud.actions = {0: [act_ud_1_ignore, act_ud_1_repair]}
ud.reward = 5.2

du = sdp.Location()
du.enabled = [1]
du.actions = {1: [act_du_2_ignore, act_du_2_repair]}
du.reward = 5.2

ur = sdp.Location()
ur.enabled = [0, 2]
ur.actions = {0: [act_r_repair],
              2: [act_ur_r]}
ur.reward = 4.5

ru = sdp.Location()
ru.enabled = [1, 2]
ru.actions = {1: [act_r_repair],
              2: [act_ru_r]}
ru.reward = 4.5

rr = sdp.Location()
rr.enabled = [2]
rr.actions = {2: [act_rr_r]}
rr.reward = 0

dd = sdp.Location()
dd.enabled = []
dd.reward = 0

p = sdp.StochasticDecisionProcess()
p.clocks = [c_1, c_2, c_r]
p.locations = [uu, ud, du, dd, ru, ur, rr]

print("Computing POMDP representation…")
m = p.pomdp_representation()

gamma = 0.8
values, policy, q_values = m.value_iteration(gamma, 1e-5)

reachable = m.reachable(0)

for s in reachable:
    l, c, st = p.reverse_translate(s)
    action = ""
    if len(m.actions[s]) > 1:
        if policy[s] == 0:
            action = "ignore"
        elif policy[s] == 1:
            action = "repair"
        if p.locations[l] == uu:
            location = "uu"
        elif p.locations[l] == ud:
            location = "ud"
        elif p.locations[l] == du:
            location = "du"
        else:
            location = "undefined"
        print("location {0} ({3}: {4} {5}), proc #{1} failure, action: {2} ({6})".
              format(location, c, action, s, l, st, policy[s]))
        print("value of actions: {0}".format(q_values[s]))

# print("Computing T-MDP representation…")
# m2 = p.t_mdp_represenatation(1, gamma)
#  t_values, t_policy, t_q_values = m2.value_iteration(gamma, 1e-5)
print("Computing SG representation…")
c_1.mD_0 /= 10
c_2.mD_0 /= 10
c_r.mD_0 /= 10
game = p.game_representation(gamma)
results = game.value_iteration(gamma, 1e-3)
v, p = results