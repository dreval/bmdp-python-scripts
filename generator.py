#!/bin/env python3
"""
An MDP generator, as an executable.

Does things.

"""
from optparse import OptionParser
import numpy as np


# make an MDP with reward variances
def create_variance_reward_process(num_states, num_actions, decimals):
    """
    Creates a randomly generated MDP in text format
    """
    REWARD_RANGE = 20
    SIGMA_RANGE = 5
    complete_string = "A random MDP with variances\n{0} {1}\n".format(num_states, num_actions)
    for a in range(num_actions):
        random_matrix = np.random.rand(num_states, num_states)
        random_matrix = random_matrix / sum(random_matrix)
        random_matrix = random_matrix.round(decimals)
        random_matrix[0, :] += (1 - sum(random_matrix))
        random_matrix = random_matrix.T

        reward_vector = (np.random.rand(num_states,) * REWARD_RANGE).round(decimals)
        sigma_vector = (np.random.rand(num_states,) * SIGMA_RANGE).round(decimals)

        for st in range(num_states):
            state_string = "{0} {1} ".format(reward_vector[st], sigma_vector[st])
            state_string += " ".join([str(random_matrix[st, s]) for s in range(num_states)])
            complete_string += (state_string + "\n")
    return complete_string


if __name__ == "__main__":
    parser = OptionParser(usage="usage: %prog [-d n] num_states num_actions")
    parser.add_option("-d", "--decimals", dest="decimals",
                      default="1000",
                      help="use at most N decimal signs",
                      metavar="N")
    (options, args) = parser.parse_args()
    if len(args) < 2:
        print("arguments expected")
        exit(1)
    n_states = int(args[0])
    n_actions = int(args[1])
    print(create_variance_reward_process(n_states, n_actions, int(options.decimals)))
