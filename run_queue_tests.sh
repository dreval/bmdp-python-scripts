#!/usr/bin/env bash

# define variables
OUTPUT_DIR=testcases_queue
BINDIR=/home/scheftel/projects/BMDP
STATISTICS_FILE=queue_ipt_statistics
ALTERNATIVE_STATISTICS_FILE=queue_evo_statistics
COMPARISON_FILE=ipt_evo_comparison
TEMP_FILE=temp

# some constants
COMPUTE_FRONT_OPTION=805
ALTERNATIVE_COMPUTE_FRONT_OPTION=815
GAMMA="0.9"

# create directory
mkdir -p ${OUTPUT_DIR}

# create output file
#echo "Size; Length; Servers; Time; Policies" > ${OUTPUT_DIR}/${STATISTICS_FILE}
#echo "Size; Length; Servers; Time; Policies" > ${OUTPUT_DIR}/${ALTERNATIVE_STATISTICS_FILE}
echo "Size; Length; Servers; Testcase; C(IPT, EVO); C(EVO, IPT); covered by IPT; total evo; covered by evo; total IPT" > ${OUTPUT_DIR}/${COMPARISON_FILE}

queue_length_list="2 3 4 5 6"
servers_list="1 2 3"
testcase_list="1 2 3 4"

for length in $queue_length_list
do
    for servers in $servers_list
    do
        echo "Model size: M/M/${servers}/${length}"
        for testcase in $testcase_list
        do
            echo "test case: ${testcase}"
            basename=testcase_queue_s${servers}_l${length}_${testcase}
            if [ ! -f "${OUTPUT_DIR}/${basename}.mdpu" ]; then
                echo "Testcase not found, regenerating."
                ./imdp_generator.py --queue -s ${servers} -l ${length} > ${OUTPUT_DIR}/${basename}.mdpu
            fi
            size=$(( (${length}+1)*(${servers}+1)*(${servers}+2)/2 ))
            time_format="${size}; ${length}; ${servers}; %e; "
            if [ ! -f "${OUTPUT_DIR}/${basename}_${COMPUTE_FRONT_OPTION}.pareto_vals" ]; then
                echo "Results of ${COMPUTE_FRONT_OPTION} not found, regenerating."
                /usr/bin/time --format="${time_format}" --output=${OUTPUT_DIR}/${TEMP_FILE} $BINDIR/BMDPanalysis ${OUTPUT_DIR}/${basename} ${COMPUTE_FRONT_OPTION} ${GAMMA}
                policies=$((`wc -l ${OUTPUT_DIR}/${basename}_${COMPUTE_FRONT_OPTION}.pareto_stat | cut -d " " -f 1 -` - 2))
                time_output=`cat ${OUTPUT_DIR}/${TEMP_FILE}`
                echo $time_output $policies >> $OUTPUT_DIR/$STATISTICS_FILE
            fi

            if [ ! -f "${OUTPUT_DIR}/${basename}_${ALTERNATIVE_COMPUTE_FRONT_OPTION}.pareto_vals" ]; then
                echo "Generating results of ${ALTERNATIVE_COMPUTE_FRONT_OPTION}."
                /usr/bin/time --format="${time_format}" --output=${OUTPUT_DIR}/${TEMP_FILE} $BINDIR/BMDPanalysis ${OUTPUT_DIR}/${basename} ${ALTERNATIVE_COMPUTE_FRONT_OPTION} ${GAMMA}
                policies=$((`wc -l ${OUTPUT_DIR}/${basename}_${ALTERNATIVE_COMPUTE_FRONT_OPTION}.pareto_stat | cut -d " " -f 1 -` - 2))
                time_output=`cat ${OUTPUT_DIR}/${TEMP_FILE}`
                echo $time_output $policies >> $OUTPUT_DIR/$ALTERNATIVE_STATISTICS_FILE
            fi

            echo "Performing comparison..."
            c_prefix="${size}; ${length}; ${servers}; ${testcase}; "
            c_measures=`python3 compare_results.py --c-measure ${OUTPUT_DIR}/${basename}_${COMPUTE_FRONT_OPTION}.pareto_vals ${OUTPUT_DIR}/${basename}_${ALTERNATIVE_COMPUTE_FRONT_OPTION}.pareto_vals`
            echo $c_prefix $c_measures >> $OUTPUT_DIR/$COMPARISON_FILE
        done
    done
done
