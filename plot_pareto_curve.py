#!/usr/bin/env python3
# -*-coding: utf-8-*-

import numpy as np
from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from optparse import OptionParser

if __name__ == "__main__":
    parser = OptionParser(usage="usage: %prog [-f file]")
    parser.add_option("-f", "--file", action="store", type="string", dest="filename")
    (options, args) = parser.parse_args()

    data = np.loadtxt(options.filename, skiprows=2)

    # first, process the columns
    indexes = data[:, 0]
    min_gain = data[:, 1]
    avg_gain = data[:, 2]
    max_gain = data[:, 3]
    gains = [min_gain, avg_gain, max_gain]

    # compute quality of the solution
    max_min_distance = 0.0
    avg_min_distance = 0.0
    distances = np.zeros((len(indexes), len(indexes)))

    for i in range(len(indexes)):
        min_distance = 0.0
        if i > 0:
            min_distance = np.min(distances[0:i, i])
        for j in range(i+1, len(indexes)):
            distance = np.sqrt(sum([(gains[k][i] - gains[k][j]) ** 2 for k in range(3)]))
            distances[i, j] = distances[j, i] = distance
            min_distance = np.minimum(min_distance, distance)

        avg_min_distance += min_distance
        max_min_distance = np.maximum(max_min_distance, min_distance)

    avg_min_distance /= len(indexes)

    print("size: {3}, average nearest neighbor: {0}, farthest nearest neighbor: {1}, relative gap: {2}".format(
        avg_min_distance, max_min_distance, max_min_distance / avg_min_distance, len(indexes)))

    # then, visualize something
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.scatter(min_gain, avg_gain, max_gain)
    ax.set_xlabel("min")
    ax.set_ylabel("avg")
    ax.set_zlabel("max")
    plt.show()