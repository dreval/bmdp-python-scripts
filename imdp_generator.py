#!/usr/bin/env python3
"""
An IMDP generator, as an executable.

Does things.

"""
from optparse import OptionParser
import imdp

__author__ = 'Dimitri Scheftelowitsch'

if __name__ == "__main__":

    parser = OptionParser(
        usage="usage: %prog num_states num_actions difference avg var")
    parser.add_option("-g", "--grid", action="store_true", dest="grid_model")
    parser.add_option("-w", "--grid-width", action="store", type="int",
                      dest="grid_width")
    parser.add_option("-d", "--grid-depth", action="store", type="int",
                      dest="grid_depth")
    parser.add_option("-q", "--queue", action="store_true", dest="queue_model")
    parser.add_option("-s", "--servers", action="store", type="int",
                      dest="num_servers")
    parser.add_option("-l", "--length", action="store", type="int",
                      dest="queue_length")
    parser.add_option("-t", "--tour-guide", action="store_true",
                      dest="tour_guide_model")
    parser.add_option("-u", "--uncertainty", action="store", type="float",
                      dest="tour_uncertainty", default=0.2)
    (options, args) = parser.parse_args()
    # do some sanity checks
    if (options.grid_model is not None) + (options.queue_model is not None) +\
            (options.tour_guide_model is not None) > 1:
        print("Error: conflicting flags, -g, -t, and -q are mutually exclusive")

    if options.grid_model or options.queue_model or options.tour_guide_model:
        mdp = None
        if options.grid_model:
            mdp = imdp.create_random_grid_model(options.grid_width,
                                                options.grid_depth)
        elif options.queue_model:
            mdp = imdp.create_random_queueing_model(options.num_servers,
                                                    options.queue_length)
        elif options.tour_guide_model:
            mdp = imdp.create_tour_guide_model(options.grid_width,
                                               options.tour_uncertainty)
        print(imdp.dump_imdp(mdp))
    else:
        if len(args) < 5:
            print("arguments expected")
            exit(1)
        n_states = int(args[0])
        n_actions = int(args[1])
        difference = float(args[2])
        avg_reward = float(args[3])
        var_reward = float(args[4])
        mdp = imdp.create_random_imdp(n_states, n_actions, difference,
                                      avg_reward, var_reward)
        print(mdp)
