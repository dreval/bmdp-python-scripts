#!/usr/bin/env bash
# A general script for testing BMDP algorithms

# where is the binary?
BINDIR=/home/scheftel/projects/BMDP
# what is its name?
BINARY=BMDPanalysis
# extra arguments?
COMPUTE_FRONT_OPTION=805
GAMMA=0.9

# the test case generator
GENERATOR_DIR=/home/scheftel/projects/bmdp-in-python
GENERATOR_BINARY=imdp_generator.py

OUTPUT_DIR="testcases_tour"
FILENAME_PATTERN="tour_%s"

STATISTICS_FILE=tour_ipt_statistics
ALTERNATIVE_STATISTICS_FILE=tour_evo_statistics
COMPARISON_FILE=ipt_evo_comparison

TEMP_FILE=temp

SIZES="10 11 12 13 14 15 16 17 18 19 20"

time_format="${size}; ${width}; %e; "

# create directory
mkdir -p ${OUTPUT_DIR}

# create output file
#echo "Size; Length; Servers; Time; Policies" > ${OUTPUT_DIR}/${STATISTICS_FILE}
#echo "Size; Length; Servers; Time; Policies" > ${OUTPUT_DIR}/${ALTERNATIVE_STATISTICS_FILE}
echo "Size; Length; Servers; Testcase; C(IPT, EVO); C(EVO, IPT); covered by IPT; total evo; covered by evo; total IPT" > ${OUTPUT_DIR}/${COMPARISON_FILE}

for size in $SIZES
do
  echo "Model size: ${size}"
  basename=$(printf $FILENAME_PATTERN $size)
  model_file="${OUTPUT_DIR}/${basename}.mdpu"

  if [ ! -f $model_file  ]; then
    ${GENERATOR_DIR}/${GENERATOR_BINARY} --tour-guide --grid-width ${size} > $model_file
  fi

  states=$(( ${size}*${size} ))
  time_format="${states}; ${size}; %e; "
  if [ ! -f "${OUTPUT_DIR}/${basename}_${COMPUTE_FRONT_OPTION}.pareto_vals" ]; then
    echo "Results of ${COMPUTE_FRONT_OPTION} not found, regenerating."
    echo "Model file: ${OUTPUT_DIR}/${basename}"
    /usr/bin/time --format="${time_format}" --output=${OUTPUT_DIR}/${TEMP_FILE} $BINDIR/BMDPanalysis ${OUTPUT_DIR}/${basename} ${COMPUTE_FRONT_OPTION} ${GAMMA}
    policies=$((`wc -l ${OUTPUT_DIR}/${basename}_${COMPUTE_FRONT_OPTION}.pareto_stat | cut -d " " -f 1 -` - 2))
    time_output=`cat ${OUTPUT_DIR}/${TEMP_FILE}`
    echo $time_output $policies >> $OUTPUT_DIR/$STATISTICS_FILE
  fi
done 
